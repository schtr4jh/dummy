# LFW #

LFW (LightFrameWork) is web framework built on top of PHP 5.5.

# VERSION #

v0.9a - current
v1 - in development

# KEYWORDS #

Check all file names in ./lib/LFW/ ;-)

# How do I get set up? #

Simply clone repository, change configuration (./config/), create and import database (./db/import.sql) and you're ready to go.

# Dependencies? #

Yup, composer. I'm sure you know how to use it.

# How to run tests #

Wait, what tests? They're coming in v1.1.

# Deployment instructions #

Same as setup.

# How to start? #
## Creating project ##
Create project folder.
```
#!
mkdir ./myproject/

```

Clone GIT repository.
```
#!
cd ./myproject/
git clone git@bitbucket.org:schtr4jh/dummy.git .

```
Update Composer dependencies.
```
#!
php composer.phar update

```
Then you simply change configuration in ./app/dummy/config/database.yml and you're ready to go.
