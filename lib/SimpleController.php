<?php

class SimpleController extends \LFW\Controller {
  protected $_one = null;
  protected $_multiple = null;
  protected $_url = null;
  protected $_i18n = false;

  function __construct() {
    parent::__construct();
  }

  function setRecordByRouterID() {
    $this->setRecordByID($this->getRouter()->get("id"));
    if (!$this->record) throw new \Exception("Record not found");

  }

  function setRecordByID($id) {
    return $this->record = $this->getEntity()->findID($id);
  }

  function setFormByRecord() {
    $this->form = $this->getForm()->{$this->record->getId() ? 'editAction' : 'addAction'}($this->record);
  }

  function getEntity($e = null) {
    $entity = parent::getEntity($e);
    if ($entity && !$e && $this->_i18n) {
      $entity->i18n();
    }
    return $entity;
  }

  function fetchRequest() {
    $_maestro = new \Weblab\Maestro\Controller\Maestro();

    if ($this->getRequest()->isPost()) {
      if ($this->form->isValidPost($invalid)) {
        $this->record->setArray($this->form->getData($this->record));

        $_maestro->{$this->record->getId() ? 'update' : 'insert'}($this->record, $this->_url, true);
      } else {
        $this->form->showRequestData()->showErrors();
      }
    }
  }

  function allWhereAction($where = array()){
    $_maestro = new \Weblab\Maestro\Controller\Maestro();

    $entity = $this->getEntity();
    $arrConstants = array('slug', 'title', 'name', 'key');
    $arrHeading = array('id' => '#');

    foreach ($arrConstants AS $constant) {
      if (defined(get_class($entity) . '::' . strtoupper($constant))) {
        $arrHeading[$constant] = $constant;
      }
    }

    return $_maestro->allForeign($entity->whereArr($where)->findAll(), array(
      "title" => $this->_multiple,
      "heading" => $arrHeading
    ));
  }
  
  function allAction(){
    $_maestro = new \Weblab\Maestro\Controller\Maestro();
    $entity = $this->getEntity();
    $arrConstants = array('slug', 'title', 'name', 'key');
    $arrHeading = array('id' => '#');

    foreach ($arrConstants AS $constant) {
      if (defined(get_class($entity) . '::' . strtoupper($constant))) {
        $arrHeading[$constant] = $constant;
      }
    }

    return $_maestro->all($this->getEntity()->findAll(), array(
      "title" => $this->_multiple,
      "heading" => $arrHeading,
    ));
  }

  function addPrepare() {
    $this->record = $this->getRecord();
    $this->setFormByRecord();
  }

  function addAction($_data = array()) {
    $this->fetchRequest();
    $_maestro = new \Weblab\Maestro\Controller\Maestro();

    if (!isset($_data['title'])) $_data['title'] = __($this->_one);
    if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();
    
    return $_maestro->add($_data);
  }

  function editPrepare() {
    $this->setRecordByRouterID();
    $this->setFormByRecord();
  }

  function editAction($_data = array()) {
    $this->fetchRequest();
    $_maestro = new \Weblab\Maestro\Controller\Maestro();

    if (!isset($_data['title'])) $_data['title'] = __($this->_one);
    if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();
    
    return $_maestro->edit($_data);
  }

  function deletePrepare() {
    $this->setRecordByRouterID();
  }

  function deleteAction($_maestro) {
    return $_maestro->delete($this->record, $this->_url, false);
  }

  function viewPrepare() {
    $this->setRecordByRouterID();
    $this->setFormByRecord();
  }

  function viewAction() {
    $_maestro = new \Weblab\Maestro\Controller\Maestro();

    return $_maestro->view(array(), array(
      "title" => __($this->_one),
      "content" => '@T00D00',
    ));
  }
}