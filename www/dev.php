<?php

// security check
if (!in_array($_SERVER['REMOTE_ADDR'], array('192.168.56.1', '192.168.11.1', '84.255.203.125', '93.103.27.78')))
  die("You're not allowed to development environment.");

// include composer autoloader
$loader = require_once "../vendor/autoload.php";

try {
	new \LFW\Core(null, "dev");
} catch (\Exception $e) {
	var_dump($e);
}

?>