<?php

// include composer autoloader
$loader = require_once "../vendor/autoload.php";

try {
	new \LFW\Core();
} catch (\Exception $e) {
	die('<!-- \LFW\Core exception -->');
}

?>