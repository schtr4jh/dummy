<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class Permissions extends \LFW\Entity {
  protected $table = "permissions";

  protected $belongsTo = array(
   "status" => array(
      "foreign" => "id",
      "current" => "status_id",
      "table" => "user_statuses",
      "entity" => "\Base\Entity\UserStatuses",
    ),
   "user" => array(
      "foreign" => "id",
      "current" => "user_id",
      "table" => "users",
      "entity" => "\Base\Entity\Users",
    ),
   "route" => array(
      "foreign" => "id",
      "current" => "route_id",
      "table" => "routes",
      "entity" => "\Base\Entity\Routes",
    ),
	);
  protected $hasMany = array();

  protected $collection = "\Base\Record\Permission";
  
	const ID = 'permissions.id';
	const STATUS_ID = 'permissions.status_id';
	const USER_ID = 'permissions.user_id';
	const ROUTE_ID = 'permissions.route_id';
	const ENABLED = 'permissions.enabled';


  function __construct() {
    parent::__construct();
  }
}
