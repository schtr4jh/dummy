<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class PicturesI18n extends \LFW\Entity {
  protected $table = "pictures_i18n";

  protected $belongsTo = array(
   "language" => array(
      "foreign" => "id",
      "current" => "language_id",
      "table" => "languages",
      "entity" => "\Base\Entity\Languages",
    ),
   "picture" => array(
      "foreign" => "id",
      "current" => "picture_id",
      "table" => "pictures",
      "entity" => "\Base\Entity\Pictures",
    ),
   "i18n" => array(
      "foreign" => "id",
      "current" => "",
      "table" => "pictures",
      "entity" => "\Base\Entity\Pictures",
    ),
	);
  protected $hasMany = array();

  protected $collection = "\Base\Record\PictureI18n";
  
	const ID = 'pictures_i18n.id';
	const TITLE = 'pictures_i18n.title';
	const LANGUAGE_ID = 'pictures_i18n.language_id';
	const PICTURE_ID = 'pictures_i18n.picture_id';


  function __construct() {
    parent::__construct();
  }
}
