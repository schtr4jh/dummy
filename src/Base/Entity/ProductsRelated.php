<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class ProductsRelated extends \LFW\Entity {
  protected $table = "products_related";

  protected $belongsTo = array(
   "product" => array(
      "foreign" => "id",
      "current" => "product_id",
      "table" => "products",
      "entity" => "\Base\Entity\Products",
    ),
   "product2" => array(
      "foreign" => "id",
      "current" => "product2_id",
      "table" => "products",
      "entity" => "\Base\Entity\Products",
    ),
	);
  protected $hasMany = array();

  protected $collection = "\Base\Record\ProductsRelated";
  
	const ID = 'products_related.id';
	const PRODUCT_ID = 'products_related.product_id';
	const PRODUCT2_ID = 'products_related.product2_id';


  function __construct() {
    parent::__construct();
  }
}
