<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class ProductColors extends \LFW\Entity {
  protected $table = "product_colors";

  protected $belongsTo = array();
  protected $hasMany = array();

  protected $collection = "\Base\Record\ProductColor";
  
	const ID = 'product_colors.id';
	const SLUG = 'product_colors.slug';


  function __construct() {
    parent::__construct();
  }
}
