<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class MailsI18n extends \LFW\Entity {
  protected $table = "mails_i18n";

  protected $belongsTo = array(
   "mail" => array(
      "foreign" => "id",
      "current" => "mail_id",
      "table" => "mails",
      "entity" => "\Base\Entity\Mails",
    ),
   "language" => array(
      "foreign" => "id",
      "current" => "language_id",
      "table" => "languages",
      "entity" => "\Base\Entity\Languages",
    ),
   "i18n" => array(
      "foreign" => "id",
      "current" => "",
      "table" => "mails",
      "entity" => "\Base\Entity\Mails",
    ),
	);
  protected $hasMany = array();

  protected $collection = "\Base\Record\MailI18n";
  
	const ID = 'mails_i18n.id';
	const MAIL_ID = 'mails_i18n.mail_id';
	const LANGUAGE_ID = 'mails_i18n.language_id';
	const TITLE = 'mails_i18n.title';
	const CONTENT = 'mails_i18n.content';
	const FROM = 'mails_i18n.from';


  function __construct() {
    parent::__construct();
  }
}
