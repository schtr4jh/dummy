<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class ProductGroupsI18n extends \LFW\Entity {
  protected $table = "product_groups_i18n";

  protected $belongsTo = array(
   "product_group" => array(
      "foreign" => "id",
      "current" => "product_group_id",
      "table" => "product_groups",
      "entity" => "\Base\Entity\ProductGroups",
    ),
   "language" => array(
      "foreign" => "id",
      "current" => "language_id",
      "table" => "languages",
      "entity" => "\Base\Entity\Languages",
    ),
   "i18n" => array(
      "foreign" => "id",
      "current" => "",
      "table" => "product_groups",
      "entity" => "\Base\Entity\ProductGroups",
    ),
	);
  protected $hasMany = array();

  protected $collection = "\Base\Record\ProductGroupI18n";
  
	const ID = 'product_groups_i18n.id';
	const TITLE = 'product_groups_i18n.title';
	const PRODUCT_GROUP_ID = 'product_groups_i18n.product_group_id';
	const LANGUAGE_ID = 'product_groups_i18n.language_id';


  function __construct() {
    parent::__construct();
  }
}
