<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Entity;

class CartsProducts extends \LFW\Entity {
  protected $table = "carts_products";

  protected $belongsTo = array(
   "cart" => array(
      "foreign" => "id",
      "current" => "cart_id",
      "table" => "carts",
      "entity" => "\Base\Entity\Carts",
    ),
   "product_instance" => array(
      "foreign" => "id",
      "current" => "product_instance_id",
      "table" => "product_instances",
      "entity" => "\Base\Entity\ProductInstances",
    ),
	);
  protected $hasMany = array();

  protected $collection = "\Base\Record\CartsProduct";
  
	const ID = 'carts_products.id';
	const QUANTITY = 'carts_products.quantity';
	const CART_ID = 'carts_products.cart_id';
	const PRICE = 'carts_products.price';
	const PRODUCT_INSTANCE_ID = 'carts_products.product_instance_id';


  function __construct() {
    parent::__construct();
  }
}
