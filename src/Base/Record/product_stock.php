<?php

/*
Autogenerated by LFW\Script
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class ProductStock extends \LFW\Record {
  protected $_entity = "\Base\Entity\ProductStocks";
  protected $_fields = array("id", "product_instance_id", "quantity", "dt_added");
 protected $id;
 protected $product_instance_id;
 protected $quantity = NULL;
 protected $dt_added = NULL;

 protected $_product_instance;

  
	const ID = 'product_stocks.id';
	const PRODUCT_INSTANCE_ID = 'product_stocks.product_instance_id';
	const QUANTITY = 'product_stocks.quantity';
	const DT_ADDED = 'product_stocks.dt_added';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setProductInstanceId($productInstanceId) {
      return $this->set('product_instance_id', (int)$productInstanceId);
  }

  public function getProductInstanceId() {
    return (int)$this->product_instance_id;
  }

  public function setProductInstance($productInstance, $recursive = true) {
    $this->_product_instance = $productInstance;
    if ($recursive) $this->setProductInstanceId($productInstance ? $productInstance->getId() : null, false);
    return $this;
  }

  public function getProductInstance() {
    if (!isset($this->_product_instance)) {
      $tmp = new \Base\Entity\ProductInstances();
      $this->setProductInstance($tmp->findId($this->getProductInstanceId()));
    }
    return $this->_product_instance;
  }

    public function setQuantity($quantity) {
      return $this->set('quantity', (int)$quantity);
  }

  public function getQuantity() {
    return (int)$this->quantity;
  }

    public function setDtAdded($dtAdded) {
      return $this->set('dt_added', $dtAdded);
  }

  public function getDtAdded() {
    return $this->dt_added;
  }   
}