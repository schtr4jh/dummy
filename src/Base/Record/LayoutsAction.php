<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class LayoutsAction extends \LFW\Record {
  protected $_entity = "\Base\Entity\LayoutsActions";
  protected $_fields = array("id", "layout_id", "action_id", "layouts_variable_id", "position", "config", "title");
 protected $id;
 protected $layout_id;
 protected $action_id;
 protected $layouts_variable_id = NULL;
 protected $position = NULL;
 protected $config = NULL;
 protected $title = NULL;

 protected $_layout;
 protected $_action;
 protected $_layouts_variable;

  
	const ID = 'layouts_actions.id';
	const LAYOUT_ID = 'layouts_actions.layout_id';
	const ACTION_ID = 'layouts_actions.action_id';
	const LAYOUTS_VARIABLE_ID = 'layouts_actions.layouts_variable_id';
	const POSITION = 'layouts_actions.position';
	const CONFIG = 'layouts_actions.config';
	const TITLE = 'layouts_actions.title';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setLayoutId($layoutId) {
      return $this->set('layout_id', (int)$layoutId);
  }

  public function getLayoutId() {
    return (int)$this->layout_id;
  }

  public function setLayout($layout, $recursive = true) {
    $this->_layout = $layout;
    if ($recursive) $this->setLayoutId($layout ? $layout->getId() : null, false);
    return $this;
  }

  public function getLayout() {
    if (!isset($this->_layout)) {
      $tmp = new \Base\Entity\Layouts();
      $this->setLayout($tmp->findId($this->getLayoutId()));
    }
    return $this->_layout;
  }

    public function setActionId($actionId) {
      return $this->set('action_id', (int)$actionId);
  }

  public function getActionId() {
    return (int)$this->action_id;
  }

  public function setAction($action, $recursive = true) {
    $this->_action = $action;
    if ($recursive) $this->setActionId($action ? $action->getId() : null, false);
    return $this;
  }

  public function getAction() {
    if (!isset($this->_action)) {
      $tmp = new \Base\Entity\Actions();
      $this->setAction($tmp->findId($this->getActionId()));
    }
    return $this->_action;
  }

    public function setLayoutsVariableId($layoutsVariableId) {
      return $this->set('layouts_variable_id', (int)$layoutsVariableId);
  }

  public function getLayoutsVariableId() {
    return (int)$this->layouts_variable_id;
  }

  public function setLayoutsVariable($layoutsVariable, $recursive = true) {
    $this->_layouts_variable = $layoutsVariable;
    if ($recursive) $this->setLayoutsVariableId($layoutsVariable ? $layoutsVariable->getId() : null, false);
    return $this;
  }

  public function getLayoutsVariable() {
    if (!isset($this->_layouts_variable)) {
      $tmp = new \Base\Entity\LayoutsVariables();
      $this->setLayoutsVariable($tmp->findId($this->getLayoutsVariableId()));
    }
    return $this->_layouts_variable;
  }

    public function setPosition($position) {
      return $this->set('position', (int)$position);
  }

  public function getPosition() {
    return (int)$this->position;
  }

    public function setConfig($config) {
      return $this->set('config', (string)$config);
  }

  public function getConfig() {
    return (string)$this->config;
  }

    public function setTitle($title) {
      return $this->set('title', (string)$title);
  }

  public function getTitle() {
    return (string)$this->title;
  }   
}