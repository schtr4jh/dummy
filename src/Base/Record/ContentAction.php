<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class ContentAction extends \LFW\Record {
  protected $_entity = "\Base\Entity\ContentActions";
  protected $_fields = array("id", "position", "config", "content_id", "action_id", "title");
 protected $id;
 protected $position = NULL;
 protected $config = NULL;
 protected $content_id;
 protected $action_id;
 protected $title = NULL;

 protected $_content;
 protected $_action;

  
	const ID = 'content_actions.id';
	const POSITION = 'content_actions.position';
	const CONFIG = 'content_actions.config';
	const CONTENT_ID = 'content_actions.content_id';
	const ACTION_ID = 'content_actions.action_id';
	const TITLE = 'content_actions.title';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setPosition($position) {
      return $this->set('position', (int)$position);
  }

  public function getPosition() {
    return (int)$this->position;
  }

    public function setConfig($config) {
      return $this->set('config', (string)$config);
  }

  public function getConfig() {
    return (string)$this->config;
  }

    public function setContentId($contentId) {
      return $this->set('content_id', (int)$contentId);
  }

  public function getContentId() {
    return (int)$this->content_id;
  }

  public function setContent($content, $recursive = true) {
    $this->_content = $content;
    if ($recursive) $this->setContentId($content ? $content->getId() : null, false);
    return $this;
  }

  public function getContent() {
    if (!isset($this->_content)) {
      $tmp = new \Base\Entity\Contents();
      $this->setContent($tmp->findId($this->getContentId()));
    }
    return $this->_content;
  }

    public function setActionId($actionId) {
      return $this->set('action_id', (int)$actionId);
  }

  public function getActionId() {
    return (int)$this->action_id;
  }

  public function setAction($action, $recursive = true) {
    $this->_action = $action;
    if ($recursive) $this->setActionId($action ? $action->getId() : null, false);
    return $this;
  }

  public function getAction() {
    if (!isset($this->_action)) {
      $tmp = new \Base\Entity\Actions();
      $this->setAction($tmp->findId($this->getActionId()));
    }
    return $this->_action;
  }

    public function setTitle($title) {
      return $this->set('title', (string)$title);
  }

  public function getTitle() {
    return (string)$this->title;
  }   
}