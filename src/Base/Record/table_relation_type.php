<?php

/*
Autogenerated by LFW\Script
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class TableRelationType extends \LFW\Record {
  protected $_entity = "\Base\Entity\TableRelationTypes";
  protected $_fields = array("id", "slug", "title");
 protected $id;
 protected $slug = NULL;
 protected $title = NULL;


  
	const ID = 'table_relation_types.id';
	const SLUG = 'table_relation_types.slug';
	const TITLE = 'table_relation_types.title';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setSlug($slug) {
      return $this->set('slug', (string)$slug);
  }

  public function getSlug() {
    return (string)$this->slug;
  }

    public function setTitle($title) {
      return $this->set('title', (string)$title);
  }

  public function getTitle() {
    return (string)$this->title;
  }   
}