<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class UserStatus extends \LFW\Record {
  protected $_entity = "\Base\Entity\UserStatuses";
  protected $_fields = array("id", "title");
 protected $id;
 protected $title;


  
	const ID = 'user_statuses.id';
	const TITLE = 'user_statuses.title';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setTitle($title) {
      return $this->set('title', (string)$title);
  }

  public function getTitle() {
    return (string)$this->title;
  }   
}