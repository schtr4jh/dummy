<?php

/*
Autogenerated by LFW\Script
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class RoutesAction extends \LFW\Record {
  protected $_entity = "\Base\Entity\RoutesActions";
  protected $_fields = array("id", "route_id", "action_id", "layouts_variable_id", "position", "config");
 protected $id;
 protected $route_id;
 protected $action_id;
 protected $layouts_variable_id = NULL;
 protected $position = NULL;
 protected $config = NULL;

 protected $_route;
 protected $_action;
 protected $_layouts_variable;

  
	const ID = 'routes_actions.id';
	const ROUTE_ID = 'routes_actions.route_id';
	const ACTION_ID = 'routes_actions.action_id';
	const LAYOUTS_VARIABLE_ID = 'routes_actions.layouts_variable_id';
	const POSITION = 'routes_actions.position';
	const CONFIG = 'routes_actions.config';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setRouteId($routeId) {
      return $this->set('route_id', (int)$routeId);
  }

  public function getRouteId() {
    return (int)$this->route_id;
  }

  public function setRoute($route, $recursive = true) {
    $this->_route = $route;
    if ($recursive) $this->setRouteId($route ? $route->getId() : null, false);
    return $this;
  }

  public function getRoute() {
    if (!isset($this->_route)) {
      $tmp = new \Base\Entity\Routes();
      $this->setRoute($tmp->findId($this->getRouteId()));
    }
    return $this->_route;
  }

    public function setActionId($actionId) {
      return $this->set('action_id', (int)$actionId);
  }

  public function getActionId() {
    return (int)$this->action_id;
  }

  public function setAction($action, $recursive = true) {
    $this->_action = $action;
    if ($recursive) $this->setActionId($action ? $action->getId() : null, false);
    return $this;
  }

  public function getAction() {
    if (!isset($this->_action)) {
      $tmp = new \Base\Entity\Actions();
      $this->setAction($tmp->findId($this->getActionId()));
    }
    return $this->_action;
  }

    public function setLayoutsVariableId($layoutsVariableId) {
      return $this->set('layouts_variable_id', (int)$layoutsVariableId);
  }

  public function getLayoutsVariableId() {
    return (int)$this->layouts_variable_id;
  }

  public function setLayoutsVariable($layoutsVariable, $recursive = true) {
    $this->_layouts_variable = $layoutsVariable;
    if ($recursive) $this->setLayoutsVariableId($layoutsVariable ? $layoutsVariable->getId() : null, false);
    return $this;
  }

  public function getLayoutsVariable() {
    if (!isset($this->_layouts_variable)) {
      $tmp = new \Base\Entity\LayoutsVariables();
      $this->setLayoutsVariable($tmp->findId($this->getLayoutsVariableId()));
    }
    return $this->_layouts_variable;
  }

    public function setPosition($position) {
      return $this->set('position', (int)$position);
  }

  public function getPosition() {
    return (int)$this->position;
  }

    public function setConfig($config) {
      return $this->set('config', (string)$config);
  }

  public function getConfig() {
    return (string)$this->config;
  }   
}