<?php

/*
Autogenerated by LFW\Script
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class ProductStocksDimension extends \LFW\Record {
  protected $_entity = "\Base\Entity\ProductStocksDimensions";
  protected $_fields = array("id", "product_stock_id", "value", "product_dimension_id");
 protected $id;
 protected $product_stock_id;
 protected $value = NULL;
 protected $product_dimension_id;

 protected $_product_stock;
 protected $_product_dimension;

  
	const ID = 'product_stocks_dimensions.id';
	const PRODUCT_STOCK_ID = 'product_stocks_dimensions.product_stock_id';
	const VALUE = 'product_stocks_dimensions.value';
	const PRODUCT_DIMENSION_ID = 'product_stocks_dimensions.product_dimension_id';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setProductStockId($productStockId) {
      return $this->set('product_stock_id', (int)$productStockId);
  }

  public function getProductStockId() {
    return (int)$this->product_stock_id;
  }

  public function setProductStock($productStock, $recursive = true) {
    $this->_product_stock = $productStock;
    if ($recursive) $this->setProductStockId($productStock ? $productStock->getId() : null, false);
    return $this;
  }

  public function getProductStock() {
    if (!isset($this->_product_stock)) {
      $tmp = new \Base\Entity\ProductStocks();
      $this->setProductStock($tmp->findId($this->getProductStockId()));
    }
    return $this->_product_stock;
  }

    public function setValue($value) {
      return $this->set('value', (string)$value);
  }

  public function getValue() {
    return (string)$this->value;
  }

    public function setProductDimensionId($productDimensionId) {
      return $this->set('product_dimension_id', (int)$productDimensionId);
  }

  public function getProductDimensionId() {
    return (int)$this->product_dimension_id;
  }

  public function setProductDimension($productDimension, $recursive = true) {
    $this->_product_dimension = $productDimension;
    if ($recursive) $this->setProductDimensionId($productDimension ? $productDimension->getId() : null, false);
    return $this;
  }

  public function getProductDimension() {
    if (!isset($this->_product_dimension)) {
      $tmp = new \Base\Entity\ProductDimensions();
      $this->setProductDimension($tmp->findId($this->getProductDimensionId()));
    }
    return $this->_product_dimension;
  }   
}