<?php

/*
Autogenerated by LFW\Script
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class TableFieldType extends \LFW\Record {
  protected $_entity = "\Base\Entity\TableFieldTypes";
  protected $_fields = array("id", "slug", "title");
 protected $id;
 protected $slug = NULL;
 protected $title = NULL;


  
	const ID = 'table_field_types.id';
	const SLUG = 'table_field_types.slug';
	const TITLE = 'table_field_types.title';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setSlug($slug) {
      return $this->set('slug', (string)$slug);
  }

  public function getSlug() {
    return (string)$this->slug;
  }

    public function setTitle($title) {
      return $this->set('title', (string)$title);
  }

  public function getTitle() {
    return (string)$this->title;
  }   
}