<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class ProductGroupI18n extends \LFW\Record {
  protected $_entity = "\Base\Entity\ProductGroupsI18n";
  protected $_fields = array("id", "title", "product_group_id", "language_id");
 protected $id;
 protected $title = NULL;
 protected $product_group_id;
 protected $language_id;

 protected $_product_group;
 protected $_language;

  
	const ID = 'product_groups_i18n.id';
	const TITLE = 'product_groups_i18n.title';
	const PRODUCT_GROUP_ID = 'product_groups_i18n.product_group_id';
	const LANGUAGE_ID = 'product_groups_i18n.language_id';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setTitle($title) {
      return $this->set('title', (string)$title);
  }

  public function getTitle() {
    return (string)$this->title;
  }

    public function setProductGroupId($productGroupId) {
      return $this->set('product_group_id', (int)$productGroupId);
  }

  public function getProductGroupId() {
    return (int)$this->product_group_id;
  }

  public function setProductGroup($productGroup, $recursive = true) {
    $this->_product_group = $productGroup;
    if ($recursive) $this->setProductGroupId($productGroup ? $productGroup->getId() : null, false);
    return $this;
  }

  public function getProductGroup() {
    if (!isset($this->_product_group)) {
      $tmp = new \Base\Entity\ProductGroups();
      $this->setProductGroup($tmp->findId($this->getProductGroupId()));
    }
    return $this->_product_group;
  }

    public function setLanguageId($languageId) {
      return $this->set('language_id', (int)$languageId);
  }

  public function getLanguageId() {
    return (int)$this->language_id;
  }

  public function setLanguage($language, $recursive = true) {
    $this->_language = $language;
    if ($recursive) $this->setLanguageId($language ? $language->getId() : null, false);
    return $this;
  }

  public function getLanguage() {
    if (!isset($this->_language)) {
      $tmp = new \Base\Entity\Languages();
      $this->setLanguage($tmp->findId($this->getLanguageId()));
    }
    return $this->_language;
  }   
}