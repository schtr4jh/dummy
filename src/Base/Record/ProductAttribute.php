<?php

/*
Autogenerated by LFW\Generator
All changes to this file will be lost at next autogenerate.
*/

namespace Base\Record;

class ProductAttribute extends \LFW\Record {
  protected $_entity = "\Base\Entity\ProductAttributes";
  protected $_fields = array("id", "slug", "product_attribute_type_id");
 protected $id;
 protected $slug = NULL;
 protected $product_attribute_type_id;

 protected $_product_attribute_type;

  
	const ID = 'product_attributes.id';
	const SLUG = 'product_attributes.slug';
	const PRODUCT_ATTRIBUTE_TYPE_ID = 'product_attributes.product_attribute_type_id';


  function __construct() {
    parent::__construct();
  }

    public function setId($id) {
      return $this->set('id', (int)$id);
  }

  public function getId() {
    return (int)$this->id;
  }

    public function setSlug($slug) {
      return $this->set('slug', (string)$slug);
  }

  public function getSlug() {
    return (string)$this->slug;
  }

    public function setProductAttributeTypeId($productAttributeTypeId) {
      return $this->set('product_attribute_type_id', (int)$productAttributeTypeId);
  }

  public function getProductAttributeTypeId() {
    return (int)$this->product_attribute_type_id;
  }

  public function setProductAttributeType($productAttributeType, $recursive = true) {
    $this->_product_attribute_type = $productAttributeType;
    if ($recursive) $this->setProductAttributeTypeId($productAttributeType ? $productAttributeType->getId() : null, false);
    return $this;
  }

  public function getProductAttributeType() {
    if (!isset($this->_product_attribute_type)) {
      $tmp = new \Base\Entity\ProductAttributeTypes();
      $this->setProductAttributeType($tmp->findId($this->getProductAttributeTypeId()));
    }
    return $this->_product_attribute_type;
  }   
}