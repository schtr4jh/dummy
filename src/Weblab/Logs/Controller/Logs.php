<?php

namespace Weblab\Logs\Controller;

class Logs extends \SimpleController {
	function __construct() {
		parent::__construct();
	}

	/* maestro */
	function allAction($_maestro){
		return $_maestro->all($this->getEntity()->findAll(), array(
			"title" => "log",
			"heading" => array(
				"id" => "#",
				"content" => "Zapis",
				"datetime" => "Čas",
				"ip" => "IP",
			),
			"headbtn" => array(),
			"rowbtn" => array()
		));
	}
}

?>