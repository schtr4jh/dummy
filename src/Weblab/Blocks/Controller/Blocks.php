<?php

namespace Weblab\Blocks\Controller;

use \Weblab\Blocks\Entity\Blocks AS EBlocks;

class Blocks extends \LFW\Controller {
	function getContentAction($_data) {
		return EBlocks::inst()->where(EBlocks::SLUG, $_data['slug'])->findOne();
	}
}