<?php

namespace Weblab\Blocks\Record;

class Block extends \Base\Record\Block {
	protected $_entity = '\Weblab\Blocks\Entity\Blocks';

	function __toString() {
		return $this->getSlug();
	}
}