<?php

namespace Weblab\Menus\Record;

use Weblab\Menus\Entity\Menus;

class Menu extends \Base\Record\Menu {
	protected $_entity = '\Weblab\Menus\Entity\Menus';

	protected static $_allMenus = array();
	protected static $_submenus = array();

	protected static function fillAllMenus($refill = false) {
		if ($refill || empty(self::$_allMenus)) {
			self::$_allMenus = Menus::inst()->i18n()->findAll();
		}
	}

	protected static function fillSubmenus($refill = false) {
		if ($refill || empty(self::$_submenus)) {
			foreach (self::$_allMenus AS $menu) {
				if (!isset(self::$_submenus[$menu->getId()])) {
					self::$_submenus[$menu->getId()] = array();
				}

				if (!isset(self::$_submenus[$menu->getMenuId()])) {
					self::$_submenus[$menu->getMenuId()] = array();
				}

				self::$_submenus[$menu->getMenuId()][] = $menu;
			}
		}
	}

	public function getSubmenus() {
		self::fillAllMenus();
		self::fillSubmenus();

		return self::$_submenus[$this->getId()];
	}

	public function getTitle() {
		return parent::getTitle() ?: $this->getSlug();
	}
}