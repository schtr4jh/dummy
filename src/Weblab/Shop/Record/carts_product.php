<?php

namespace Weblab\Shop\Record;

use Weblab\Shop\Entity\ProductInstance;
use Weblab\Shop\Record\CartsProduct;
use Weblab\Shop\Record\OrdersProduct;
use Exception;

class CartsProduct extends \Base\Record\CartsProduct {

	public function logMove($move) {

	}

	public function moveToOrder($order) {
		// create order product instance
		$ordersProductInstance = new OrdersProduct();
		$ordersProductInstance->setProductInstanceId($this->getProductInstanceId());
		$ordersProductInstance->setQuantity($this->getQuantity());
		$ordersProductInstance->setOrderId($order->getId());

		// save orders product instance
		if (!$ordersProductInstance->save()) throw new Exception("Cannot create orders product insance");
		
		// log action
		if (!$this->logMove(ProductInstanceLogActions::CART_ORDER)) throw new Exception("Cannot write logs");

		// delete cart product instance
		if (!$productInstance->delete()) throw new Exception("Cannot delete carts product instance");

		return true;
	}
}