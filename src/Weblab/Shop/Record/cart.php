<?php

namespace Weblab\Shop\Record;

use Weblab\Shop\Entity\ProductInstances;
use Weblab\Shop\Entity\ProductInstanceLogs;
use Weblab\Shop\Entity\ProductInstanceLogActions;
use Weblab\Shop\Entity\CartsProductInstances;

use Weblab\Shop\Record\ProductInstance;
use Weblab\Shop\Record\ProductInstanceLog;
use Weblab\Shop\Record\CartsProductInstance;
use Weblab\Shop\Record\OrdersProductInstance;
use Weblab\Shop\Record\Order;
use Exception;

class Cart extends \Base\Record\Cart {
	protected $_entity = '\Weblab\Shop\Entity\Carts';

	public function addProductInstance($productInstanceId, $quantity = 1) {
		try {
			// check quantity
			if ($quantity <= 0) throw new Exception("Quantity must be bigger than 0");

			// start transaction

			// find real product instance
			$productInstance = ProductInstances::inst()->findID($productInstanceId);

			if (!$productInstance) throw new Exception("Cannot find product instance");
			if (!$productInstance->getStock()) throw new Exception("Product is not in stock");
			if ($productInstance->getStock() < $quantity) throw new Exception("Only " . $productInstance->getStock() . " product(s) available");

			// move to cart
			if (!$cartsProduct = $productInstance->moveToCart($this, $quantity)) throw new Exception("Cannot move product instance to cart");

			// save transaction
			
			return $cartsProduct;
		} catch (\Exception $e) {
			// reset transaction
			throw $e;
			//throw new \Exception("Cannot add product to cart", 0, $e);
		}

		return false;
	}

	public function makeOrder() {
		try {
			// get product instances
			if (!($arrCartsProductInstances = $this->getProductInstances())) throw new Exception("Cart is empty");

			// start transaction

			// create order
			if (!($order = Orders::createNew())) throw new Exception("Cannot create order");

			// move products from cart to order
			foreach ($arrCartsProductInstances AS $cartProductInstance) {
				$orderPrice += $cartProductInstance->getQuantity() * $cartProductInstance()->getPrice();

				// move to order
				if (!$cartProductInstance->moveToOrder($order)) throw new Exception("Cannot move product instance from cart to order");
			}

			// set all prices
			$order->setPrice($orderPrice);

			if (!$order->save()) throw new Exception("Cannot save order");

			// save transaction
			return $order;
		} catch (\Exception $e) {
			// reset transaction
			throw new \Exception("Cannot make order", $e);
		}

		return false;
	}

	public function getProductInstances() {
		return CartsProductInstances::inst()
			->where(CartsProductInstances::CART_ID, $this->getId())
			->findAll();
	}
}