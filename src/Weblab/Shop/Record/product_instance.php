<?php

namespace Weblab\Shop\Record;

use Weblab\Shop\Record\ProductInstance;
use Weblab\Shop\Record\CartsProduct;
use Weblab\Shop\Record\OrdersProductInstance;
use Weblab\Logs\Entity\LogActions;
use Exception;
use LFW\Core;

class ProductInstance extends \Base\Record\ProductInstance {
	protected $_entity = '\Weblab\Shop\Entity\ProductInstances';

	public function logMove($move) {

	}

	public function moveToCart($cart, $quantity) {
		// add product to cart
		$cartsProduct = new CartsProduct();
		$cartsProduct->setQuantity($quantity);
		$cartsProduct->setCartId($cart->getId());
		$cartsProduct->setProductInstanceId($this->getId());
		if (!$cartsProduct->save()) throw new Exception("Cannot move product from stock to cart" . print_r($cartsProduct, true));

		// update stock
		$this->changeStock(-$quantity)->save();

		// write logs
		if ($this->logMove(LogActions::STOCK_CART)) throw new Exception("Cannot write logs");

		return $cartsProduct;
	}

	public function changeStock($quantity) {
		if ($quantity == 0) { 
			return $this;
		} else if ($quantity > 0) {
			return $this->setStock($this->getStock() + $quantity);
		} else if ($quantity < 0) {
			return $this->setStock($this->getStock() + $quantity);
		}

		return false;
	}

	public function getStocks() {
		// get all dimensions
		$sqlDimensions = "SELECT DISTINCT pd.id, pd.slug " .
			"FROM product_stocks ps " .
			"INNER JOIN product_stocks_dimensions psd ON psd.product_stock_id = ps.id " .
			"INNER JOIN product_dimensions pd ON pd.id = psd.product_dimension_id " .
			"WHERE ps.product_instance_id = " . $this->getId();
		$pDimensions = \LFW\DB::inst()->prepare($sqlDimensions);
		$qDimensions = $pDimensions->execute();
		$arrDimensions = $pDimensions->fetchAll();

		// build select and joins
		$buildSelectSQL = array();
		$buildJoinSQL = array();
		$buildGroupSQL = array('ps.id');
		foreach ($arrDimensions AS $dimension) {
			$i = $dimension->slug;
			if ($i == "color") { // color
				$buildSelectSQL[] = "pc.slug AS color";
				$buildJoinSQL[] = "INNER JOIN product_stocks_dimensions psd_clr ON psd_clr.product_stock_id = ps.id ";
				$buildJoinSQL[] = "INNER JOIN product_colors pc ON (psd_clr.product_dimension_id = " . $dimension->id . " AND pc.id = psd_clr.value)";
				$buildGroupSQL[] = "pc.id";
			} else if ($i == "size") { // size
				$buildSelectSQL[] = "psize.slug AS size";
				$buildJoinSQL[] = "INNER JOIN product_stocks_dimensions psd_size ON psd_size.product_stock_id = ps.id ";
				$buildJoinSQL[] = "INNER JOIN product_sizes psize ON (psd_size.product_dimension_id = " . $dimension->id . " AND psize.id = psd_size.value)";
				$buildGroupSQL[] = "psize.id";
			} else if ($i == 3) { // size
				$buildSelectSQL[] = "pm.title AS material";
				$buildJoinSQL[] = "LEFT JOIN product_materials pm ON (psd.product_dimension_id = " . $dimension->id . " AND pm.id = psd.value)";
			} else if ($i == 4) { // date range
				$buildSelectSQL[] = "psd1.value AS from";
				$buildJoinSQL[] = "LEFT JOIN product_instances_dimensions psd1 ON (psd1.product_dimension_id = " . $dimension->id . " AND psd1.product_instance_stock_id = pis.id AND SUBSTRING(psd.value, 0, 10) <= '" . date("Y-m-d") . "' AND SUBSTRING(psd.value, 11, 10) <= '" . date("Y-m-d") . "')";
			}
		}

		$sqlStocks = "SELECT SUM(ps.quantity) AS quantity, " . implode($buildSelectSQL, ", ") . " " . // pc.title AS color, ps.title AS size, pm.title AS material, pisd4.value AS from, pisd5.value AS to
			"FROM product_stocks ps " .
			implode($buildJoinSQL, " \n") . " " .
			"WHERE ps.product_instance_id = " . $this->getId() . " " .
			($buildGroupSQL ? "GROUP BY " . implode(", ", $buildGroupSQL) : "");
		$pStocks = \LFW\DB::inst()->prepare($sqlStocks);
		$qStocks = $pStocks->execute();
		$arrStocks = $pStocks->fetchAll();
		foreach ($arrStocks AS &$stock) {
			$stock = (array)$stock;
		}
		return $arrStocks;
	}
}