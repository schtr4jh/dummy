<?php

namespace Weblab\Shop\Record;

use Weblab\Shop\Entity\PaymentMethods;

class Order extends Base\Entity\Order {

	public function setPrice($orderPrice = 0.0, $deliveryPrice = 0.0) {
		return 
			$this->setOrderPrice($orderPrice)
			&& $this->setDeliveryPrive($deliveryPrice)
			&& $this->setTotalPrice($orderPrice + $deliveryPrice);
	}

	public function startPayment() {
		$paymentMethod = PaymentMethods::init()->findID($this->getPaymentMethodId());

		if (!$paymentMethod) {
			return false;
		}

		$paymentApi = $paymentMethod->getAPI();

		if (!$paymentApi) {
			return false;
		}
	}
}