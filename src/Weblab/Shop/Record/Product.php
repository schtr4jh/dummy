<?php

namespace Weblab\Shop\Record;

use Weblab\Shop\Entity\ProductPictures;
use Weblab\Shop\Entity\ProductInstances;
use Weblab\Shop\Entity\ProductPrices;

class Product extends \Base\Record\Product {
	protected $mainPicture = null;

	public function getPicture() {
		if ($this->mainPicture) return $this->mainPicture;

		$arrPictures = $this->getPictures();

		if (!$arrPictures) return null;

		foreach ($arrPictures AS $picture) {
			if ($picture->isMain()) {
				return $this->mainPicture = $picture;
			}
		}

		return $arrPictures[0];
	}

	public function getPictures() {
		return ProductPictures::inst()->where(ProductPictures::PRODUCT_ID, $this->getId())->findAll();
	}

	public function getMinPrice() {
		$arrProductPrices = ProductPrices::getProductPrices($this->getId());

		if (!$arrProductPrices) return null;

		$minPrice = null;
		foreach ($arrProductPrices AS $price) {
			if (is_null($minPrice) || $minPrice > $price->getPrice()) {
				$minPrice = $price->getPrice();
			}
		}

		return $minPrice;
	}

	public function getMaxPrice() {
		$arrProductPrices = ProductPrices::getProductPrices($this->getId());

		if (!$arrProductPrices) return null;

		$maxPrice = null;
		foreach ($arrProductPrices AS $price) {
			if (is_null($maxPrice) || $maxPrice < $price->getPrice()) {
				$maxPrice = $price->getPrice();
			}
		}

		return $maxPrice;
	}

	public function getPrice() {
		return $this->getMinPrice();
	}

	public function priceVary() {
		$arrProductPrices = ProductPrices::getProductPrices($this->getId());

		if (!$arrProductPrices) return null;

		$varyPrice = null;
		foreach ($arrProductPrices AS $price) {
			if (!is_null($varyPrice) && $price->getPrice() != $varyPrice) {
				return true;
			}

			$varyPrice = $price->getPrice();
		}

		return false;
	}

	public function getInstances() {
		return ProductInstances::inst()->where(ProductInstances::PRODUCT_ID, $this->getId())->findAll();
	}
}