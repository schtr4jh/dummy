<?php

namespace Weblab\Shop\Entity;

class Products extends \Base\Entity\Products {
	protected $collection = "\Weblab\Shop\Record\Product";
	
	public static function getAllProducts() {
		return self::inst()->i18n()->findAll();
	}
	
}

?>