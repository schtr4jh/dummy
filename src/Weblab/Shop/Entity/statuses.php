<?php

namespace Weblab\Shop\Entity;

class Statuses extends \Base\Entity\Statuses {
	const ORDER_STATUS_ID = 2;
	const PAYMENT_STATUS_ID = 6;
	const DELIVERY_STATUS_ID = 7;
}