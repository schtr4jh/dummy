<?php

namespace Weblab\Shop\Entity;

use Weblab\Shop\Record\Cart;

class Carts extends \Base\Entity\Carts {
	protected $collection = '\Weblab\Shop\Record\Cart';

	public static function getCurrent() {
		$cart = Carts::inst()->where(Carts::USER_ID, 1)->findOne();

		if (!$cart) {
			$cart = new Cart();
			$cart->setUserId(1);
			$cart->setDtCreated(date("Y-m-d H:i:s"));
			$cart->save();
		}

		return $cart;
	}
}