<?php

namespace Weblab\Shop\Entity;

class ProductInstances extends \Base\Entity\ProductInstances {
	protected $collection = '\Weblab\Shop\Record\ProductInstance';

	public static function getProductPrices($productId) {
		$arrProductInstances = ProductInstances::inst()
			->where(ProductInstances::PRODUCT_ID, $productId)
			->findList();

		if (!$arrProductInstances) return null;

		return ProductPrices::inst()
			->where(ProductPrices::PRODUCT_INSTANCE_ID, $arrProductInstances)
			->findAll();
	}

}

?>