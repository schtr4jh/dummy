<?php

namespace Weblab\Shop\Entity;

class ProductPrices extends \Base\Entity\ProductPrices {

	public static function getProductPrices($productId) {
		$arrProductInstances = ProductInstances::inst()
			->where(ProductInstances::PRODUCT_ID, $productId)
			->findList();

		if (!$arrProductInstances) return null;

		return ProductPrices::inst()
			->where(ProductPrices::PRODUCT_INSTANCE_ID, $arrProductInstances)
			->findAll();
	}

}

?>