<?php

namespace Weblab\Shop\Entity;

use Weblab\Shop\Record\Order;

class Orders extends \Base\Entity\Orders {

	public static function createNew() {
		$order = new Order();
		$order->setUserId(1);
		$order->save();

		return $order;
	}
}