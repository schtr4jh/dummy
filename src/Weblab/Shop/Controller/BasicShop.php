<?php

namespace Weblab\Shop\Controller;

use LFW\Controller;
use LFW\View;

use Weblab\Shop\Entity\Products;

class BasicShop extends Controller {
	public function indexAction() {
		return new View("products", array(
			"products" => Products::getAllProducts(),
		));
	}

	public function viewProductAction() {
		return new View("product", array(
			"product" => Products::inst()->i18n()->findID($this->getRouter()->get('id')),
		));
	}
}

?>