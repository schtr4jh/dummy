<?php

namespace Weblab\Shop\Controller;

use LFW\View;

class Shop extends BasicShop {
  function allAction($_maestro) {
    return $_maestro->custom(array("content" => new View("maestro/index")));
  }
}

?>