<?php

namespace Weblab\Shop\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

use Weblab\Statuses\Entity\Statuses;
use Weblab\Users\Entity\Users;
use Base\Entity\DeliveryMethods;

class PaymentMethods extends \Htmlbuilder\Bootstrap\Form {
  function addAction($paymentMethod) {
    return $this->editAction($paymentMethod);
  }

  function editAction($paymentMethod) {
    $this->addToFieldset(array(
      new Input\ID($paymentMethod),
      $slug = new Input\Text("slug", $paymentMethod),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $paymentMethod->addValidator(Validator::REQUIRED);

    return $this;
  }
}