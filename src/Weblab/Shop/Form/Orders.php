<?php

namespace Weblab\Shop\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

use Weblab\Orders\Entity\Statuses;
use Weblab\Users\Entity\Users;
use Base\Entity\PaymentMethods;
use Base\Entity\DeliveryMethods;

class Orders extends \Htmlbuilder\Bootstrap\Form {
  function addAction($order) {
    return $this->editAction($order);
  }

  function editAction($order) {
    $this->addToFieldset(array(
      new Input\ID($order),
      $user = new Select("user_id", $order),
      $orderStatus = new Select("order_status_id", $order),
      $paymentStatus = new Select("payment_status_id", $order),
      $paymentMethod = new Select("payment_method_id", $order),
      $deliveryStatus = new Select("delivery_status_id", $order),
      $deliveryMethod = new Select("delivery_method_id", $order),
      $totalPrice = new Input\Currency("total_price", $order),
      $orderPrice = new Input\Currency("total_price", $order),
      $deliveryPrice = new Input\Currency("delivery_price", $order),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $user->setOptions(Users::inst()->findList())->setDefault();
    $orderStatus->setOptions(Statuses::inst()->where(Statuses::STATUS_ID, Statuses::ORDER_STATUS_ID)->findList())->setDefault();
    $paymentStatus->setOptions(Statuses::inst()->where(Statuses::STATUS_ID, Statuses::PAYMENT_STATUS_ID)->findList())->setDefault();
    $paymentMethod->setOptions(PaymentMethods::inst()->findList())->setDefault();
    $deliveryStatus->setOptions(Statuses::inst()->where(Statuses::STATUS_ID, Statuses::DELIVERY_STATUS_ID)->findList())->setDefault();
    $deliveryMethod->setOptions(DeliveryMethods::inst()->findList())->setDefault();

    return $this;
  }

  function getData($object = null, $key = null) {
    $data = parent::getData($object, $key);

    return $data;
  }
}