<?php

namespace Weblab\Shop\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

use Weblab\Users\Entity\Users;

class DeliveryMethods extends \Htmlbuilder\Bootstrap\Form {
  function addAction($deliveryMethod) {
    return $this->editAction($deliveryMethod);
  }

  function editAction($deliveryMethod) {
    $this->addToFieldset(array(
      new Input\ID($deliveryMethod),
      $slug = new Input\Text("slug", $deliveryMethod),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $slug->addValidator(Validator::REQUIRED);

    return $this;
  }
}