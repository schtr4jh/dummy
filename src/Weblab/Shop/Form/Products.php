<?php

namespace Weblab\Shop\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

class Products extends \Htmlbuilder\Bootstrap\Form {
  function addAction($product) {
    return $this->editAction($product);
  }

  function editAction($product) {
    $this->addToFieldset(array(
      new Input\ID($product),
      $slug = new Input\Text("slug", $product),
      $group = new Select("product_group_id", $product),
      $brand = new Select("product_brand_id", $product),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $slug->addValidator(Validator::REQUIRED);

    $group->setOptions(\Base\Entity\ProductGroups::inst()->findList())->addValidator(Validator::REQUIRED);
    $brand->setOptions(\Base\Entity\ProductBrands::inst()->findList())->addValidator(Validator::REQUIRED);

    return $this;
  }

  function getData($object = null, $key = null) {
    $data = parent::getData($object, $key);

    return $data;
  }
}