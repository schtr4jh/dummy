<?php

namespace Weblab\Dynamic\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

class Relations extends \Htmlbuilder\Bootstrap\Form {
  function addAction($relation){
    return $this->editAction($relation);
  }

  function editAction($relation) {
    $this->addToFieldset(array(
      new Input\ID($relation),
      $title = new Input\Text("title", $relation),
      $table1 = new Select("table1_id", $relation),
      $tableField1 = new Select("table_field1_id", $relation),
      $relationType = new Select("table_relation_type_id", $relation),
      $table2 = new Select("table2_id", $relation),
      $tableField2 = new Select("table_field2_id", $relation),

      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $title->addValidator(Validator::REQUIRED);

    $table1->setOptions(\Weblab\Dynamic\Entity\Tables::inst()->findList())->addValidator(Validator::REQUIRED);
    $tableField1->setOptions(\Weblab\Dynamic\Entity\Fields::inst()->makeSyncList())->addValidator(Validator::REQUIRED);
    $relationType->setOptions(\Weblab\Dynamic\Entity\RelationTypes::inst()->findList())->addValidator(Validator::REQUIRED);
    $table2->setOptions(\Weblab\Dynamic\Entity\Tables::inst()->findList())->addValidator(Validator::REQUIRED);
    $tableField2->setOptions(\Weblab\Dynamic\Entity\Fields::inst()->makeSyncList())->addValidator(Validator::REQUIRED);

    return $this;
  }
}