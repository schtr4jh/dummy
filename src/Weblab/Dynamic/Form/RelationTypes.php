<?php

namespace Weblab\Dynamic\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

class RelationTypes extends \Htmlbuilder\Bootstrap\Form {
  function addAction($relationType){
    return $this->editAction($relationType);
  }

  function editAction($relationType) {
    $this->addToFieldset(array(
      new Input\ID($relationType),
      $title = new Input\Text("title", $relationType),
      $slug = new Input\Text("slug", $relationType),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $title->addValidator(Validator::REQUIRED);
    $slug->addValidator(Validator::REQUIRED);

    return $this;
  }
}