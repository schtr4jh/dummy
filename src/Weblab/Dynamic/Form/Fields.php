<?php

namespace Weblab\Dynamic\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

class Fields extends \Htmlbuilder\Bootstrap\Form {
  function addAction($field){
    return $this->editAction($field);
  }

  function editAction($field) {
    $this->addToFieldset(array(
      new Input\ID($field),
      $title = new Input\Text("title", $field),
      $field2 = new Input\Text("field", $field),
      $fieldType = new Select("table_field_type_id", $field),
      $table = new Select("table_id", $field),
      $shown = new Input\Checkbox("shown", $field),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $title->addValidator(Validator::REQUIRED);
    $field2->addValidator(Validator::REQUIRED);

    $fieldType->setOptions(\Weblab\Dynamic\Entity\FieldTypes::inst()->findList())->addValidator(Validator::REQUIRED);
    $table->setOptions(\Weblab\Dynamic\Entity\Tables::inst()->findList())->addValidator(Validator::REQUIRED);

    return $this;
  }
}