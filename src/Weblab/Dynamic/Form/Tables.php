<?php

namespace Weblab\Dynamic\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

class Tables extends \Htmlbuilder\Bootstrap\Form {
  function addAction($table){
    return $this->editAction($table);
  }

  function editAction($t) {
    $this->addToFieldset(array(
      new Input\ID($t),
      $title = new Input\Text("title", $t),
      $table = new Input\Text("table", $t),
      $ent = new Input\Text("ent", $t),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $title->addValidator(Validator::REQUIRED);
    $table->addValidator(Validator::REQUIRED);

    return $this;
  }
}