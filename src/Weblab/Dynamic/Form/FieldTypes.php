<?php

namespace Weblab\Dynamic\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

class FieldTypes extends \Htmlbuilder\Bootstrap\Form {
  function addAction($table){
    return $this->editAction($table);
  }

  function editAction($t) {
    $this->addToFieldset(array(
      new Input\ID($t),
      $title = new Input\Text("title", $t),
      $slug = new Input\Text("slug", $t),
      new Group(array(new Button\Submit(), new Button\Cancel())),
    ));

    $title->addValidator(Validator::REQUIRED);
    $slug->addValidator(Validator::REQUIRED);

    return $this;
  }
}