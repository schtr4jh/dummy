<?php

namespace Weblab\Dynamic\Form;

use Htmlbuilder\Bootstrap\Input;
use Htmlbuilder\Bootstrap\Select;
use Htmlbuilder\Bootstrap\Button;
use Htmlbuilder\Bootstrap\Textarea;
use Htmlbuilder\Bootstrap\Textarea\HTML;
use Htmlbuilder\Bootstrap\Group;
use Htmlbuilder\Validator;

use \Weblab\Dynamic\Entity\Fields;
use \Weblab\Dynamic\Entity\Tables;
use \Weblab\Dynamic\Entity\Relations;

class Records extends \Htmlbuilder\Bootstrap\Form {
  protected $onFlyFieldset = array();

  function addAction($table, $record){
    return $this->editAction($table, $record);
  }

  function editAction($table, $record) {
    $arrFields = Fields::inst()->where(Fields::TABLE_ID, $table->getId())->findAll();

    if (!$record) throw new \Exception("Record not found");

    foreach ($arrFields AS $field) {
      $this->addToFieldsetBySlug($field->getTableFieldType()->getSlug(), $field, $record, $element = null);
      if ($field->isRequired()) {
        $element->addValidator(Validator::REQUIRED);
      }
    }

    $this->onFlyFieldset[] = new Group(array(new Button\Submit(), new Button\Cancel()));

    $this->addToFieldset($this->onFlyFieldset);

    return $this;
  }

  function getData($table = null, $record = null) {
    if (!$table) {
        return parent::getData($table, $key);
    }

    $arrData = parent::getData($record);
    $arrFields = Fields::inst()->where(Fields::TABLE_ID, $table->getId())->findAll();

    foreach ($arrFields AS $field) {
      $slug = $field->getTableFieldType()->getSlug();

      if ($slug == 'password') {
        if (array_key_exists($field->getField(), $arrData) && empty($arrData[$field->getField()])) {
          unset($arrData[$field->getField()]);
        } else if (array_key_exists($field->getField(), $arrData)) {
          $arrData[$field->getField()] = \LFW\Helpers\Auth::makePassword($arrData[$field->getField()]);
        }
      } else if ($slug == 'file' || $slug == 'picture') {

        $class = str_replace("\\", "_", get_class($record));
        if (isset($_FILES[$class]["name"][$field->getField()])) {
          $file = array(
            "name" => $_FILES[$class]["name"][$field->getField()],
            "type" => $_FILES[$class]["type"][$field->getField()],
            "tmp_name" => $_FILES[$class]["tmp_name"][$field->getField()],
            "error" => $_FILES[$class]["error"][$field->getField()],
            "size" => $_FILES[$class]["size"][$field->getField()],
          );

          if ($slug == 'picture') {
            $extension = strtolower(end(explode(".", $file['name'])));
            if (!in_array($extension, array("png", "jpg", "gif", "jpeg", "bmp"))) {
              break;
            }
          }

          $dir = $field->getTable()->getTable() . '/';

          if (!is_dir(UPLOADS_PATH . $dir)) {
            mkdir(UPLOADS_PATH . $dir, 0775, true);
          }

          if (!file_exists(UPLOADS_PATH . $dir . $file['name'])) {
            $prepend = '';
          } else {
            $prepend = 1;
            while (file_exists(UPLOADS_PATH . $dir . $prepend . "-" . $file['name'])) {
              $prepend++;
            }
            $prepend = $prepend . '-';
          }

          $newPath = $dir . $prepend . $file['name'];

          move_uploaded_file($file["tmp_name"], UPLOADS_PATH . $newPath);

          $arrData[$field->getField()] = $newPath;
        }
      }
    }

    return $arrData;
  }

  function addToFieldsetBySlug($slug, $field, $record, $element = NULL) {
    if ($slug == 'id') {
      $this->onFlyFieldset[] = $element = new Input\ID($record);
    } else if ($slug == 'foreign') {
      $this->onFlyFieldset[] = $element = new Select($field->getField(), $record);
      $relation = Relations::inst()
          ->where(Relations::TABLE1_ID, $field->getTableId())
          ->where(Relations::TABLE_FIELD1_ID, $field->getId())
          ->where(Relations::TABLE_RELATION_TYPE_ID, array(2,4,6))
          ->findOne();
      if (!$relation) {
        var_dump($field);die();
      } else {
        //var_dump($relation);
      }

      $relatedEntity = $relation->getTable2()->getEnt();
      $relatedEntity = new $relatedEntity;
      $element->setOptions($relatedEntity->findListID());

      if (!$field->isRequired()) {
        $element->setDefault();
      }

      $element->addClass("selectpicker");
      $element->setAttribute("data-live-search", "true");
    } else if ($slug == 'password') {
      $this->onFlyFieldset[] = $element = new Input\Password($field->getField(), $record);
    } else if ($slug == 'email') {
      $this->onFlyFieldset[] = $element = new Input\Email($field->getField(), $record);
    } else if ($slug == 'text') {
      $this->onFlyFieldset[] = $element = new Textarea($field->getField(), $record);
    } else if ($slug == 'html') {
      $this->onFlyFieldset[] = $element = new HTML($field->getField(), $record);
    } else if ($slug == 'varchar') {
      $this->onFlyFieldset[] = $element = new Input\Text($field->getField(), $record);
    } else if ($slug == 'file') {
      $this->onFlyFieldset[] = $element = new Input\File($field->getField(), $record);
    } else if ($slug == 'picture') {
      $this->onFlyFieldset[] = $element = new Input\Picture($field->getField(), $record);
    } else if ($slug == 'int') {
      $this->onFlyFieldset[] = $element = new Input\Int($field->getField(), $record);
    } else if ($slug == 'bool') {
      $this->onFlyFieldset[] = $element = new Input\Checkbox($field->getField(), $record);
    } else if ($slug == 'slug') {
      $this->onFlyFieldset[] = $element = new Input\Text($field->getField(), $record);
    } else {
      var_dump($slug);
    }

    if ($element)
      $element->setLabel($field->getTitle());

    return $element;
  }
}