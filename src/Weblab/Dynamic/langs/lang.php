<?php

namespace Weblab\Langs\Langs;

use LFW\Lang;

Lang::set(Lang::EN, "user", "User");
Lang::set(Lang::SI, "user", "Uporabnik");

Lang::set(Lang::EN, "users", "Users");
Lang::set(Lang::SI, "users", "Uporabnikov");

Lang::set(Lang::EN, "email", "Email");
Lang::set(Lang::SI, "email", "Email");

Lang::set(Lang::EN, "password", "Password");
Lang::set(Lang::SI, "password", "Geslo");

Lang::set(Lang::EN, "title", "Title");
Lang::set(Lang::SI, "title", "Ime");

Lang::set(Lang::EN, "status", "Status");
Lang::set(Lang::SI, "status", "Status");

Lang::set(Lang::EN, "statuses", "Statuses");
Lang::set(Lang::SI, "statuses", "Statusev");

//Lang::set(Lang::EN, "title", "Title");
//Lang::set(Lang::SI, "title", "Ime");

// labels

Lang::set(Lang::EN, "label_email", "Email");
Lang::set(Lang::SI, "label_email", "Email");

Lang::set(Lang::EN, "label_password", "Password");
Lang::set(Lang::SI, "label_password", "Geslo");

Lang::set(Lang::EN, "label_status_title", "Status");
Lang::set(Lang::SI, "label_status_title", "Status");

// placeholders
Lang::set(Lang::EN, "placeholder_email", "john.smith@gmail.com");
Lang::set(Lang::SI, "placeholder_email", "janez.novak@gmail.com");

Lang::set(Lang::EN, "placeholder_password", "Choose safe password");
Lang::set(Lang::SI, "placeholder_password", "Izberite varno geslo");

Lang::set(Lang::EN, "placeholder_status_title", "Title");
Lang::set(Lang::SI, "placeholder_status_title", "Naziv statusa");


?>