<?php

namespace Weblab\Dynamic\Entity;

class Fields extends \Base\Entity\TableFields {
  function findList() {
  	return $this->join(new Tables)->select('CONCAT( tables.table,  \'.\', table_fields.field ) AS title')->where(self::TABLE_FIELD_TYPE_ID, array(1,11))->findList();
  	// @T00D00
  }
}