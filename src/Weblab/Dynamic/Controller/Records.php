<?php

namespace Weblab\Dynamic\Controller;

use \Weblab\Dynamic\Entity\Relations AS ERelations;
use \Weblab\Dynamic\Entity\Tables AS ETables;

use \Weblab\Maestro\Controller\Maestro;
use \LFW\Context;

class Records extends \LFW\Controller {
  protected $_one = null;
  protected $_multiple = null;
  protected $_url = null;
  protected $_i18n = false;

  protected $_maestro = null;

  function __construct() {
    parent::__construct();

    $this->_maestro = new Maestro();
  }

  function checkPrivileges() {
    
  }

  function prepare() {
    if ($this->getRouter()->get("id")) {
      $this->table = $this->getEntity()->findId($this->getRouter()->get("id"));
      if (!$this->table) throw new \Exception("Table not found");

      $this->_one = $this->table->getTable();
      $this->_multiple = $this->table->getTable();
      $this->_url = 'dynamic/tables/' . $this->table->getId();
    }

    $this->record = $this->getRouter()->get("record")
        ? $this->getEntity($this->table->getEnt())->findId($this->getRouter()->get("record"))
        : $this->getEntity($this->table->getEnt())->getEmptyRecord();

    if (!$this->record) throw new \Exception("Record not found");
  }

  function setFormByRecord() {
    $this->form = $this->getForm()->{$this->record->getId() ? 'editAction' : 'addAction'}($this->table, $this->record);
  }

  function getEntity($e = null) {
  	if ($e) { return parent::getEntity($e); }

  	return new \Weblab\Dynamic\Entity\Tables;
  }

  function fetchRequest() {
    if (Context::getRequest()->isPost()) {
      if ($this->form->isValidPost($invalid)) {
        $this->record->setArray($this->form->getData($this->table, $this->record));

        $this->_maestro->{$this->record->getId() ? 'update' : 'insert'}($this->record, $this->_url, true);
      } else {
        $this->form->showRequestData()->showErrors();
      }
    }
  }

  function allWhereAction($where = array()){
    return self::allWhere($this->getEntity(), $where, array(
      "title" => $this->_multiple,
      "heading" => $arrHeading
    ));
  }

  static function allWhere($entity, $where, $conf) {
    $arrConstants = array('slug', 'title', 'email', 'name', 'key');
    $arrHeading = array('id' => '#');

    foreach ($arrConstants AS $constant) {
      if (defined(get_class($entity) . '::' . strtoupper($constant))) {
        $arrHeading[$constant] = $constant;
      }
    }

    $conf['heading'] = $arrHeading;

    $_maestro = new Maestro;

    return $_maestro->allForeign($entity->whereArr($where)->findAll(), $conf);
  }

  function allPrepare() {
    $this->prepare();
  }
  
  function allAction(){
    $entity = $this->getEntity();
    $arrConstants = array('slug', 'title', 'name', 'email', 'name', 'key');
    $arrHeading = array('id' => '#');

    foreach ($arrConstants AS $constant) {
      if (defined(get_class($entity) . '::' . strtoupper($constant))) {
        $arrHeading[$constant] = $constant;
      }
    }

    return $this->_maestro->all($this->getEntity()->findAll(), array(
      "title" => $this->_multiple,
      "heading" => $arrHeading,
    ));
  }

  function addPrepare() {
    $this->prepare();
    $this->setFormByRecord();
  }

  function addAction($_data = array()) {
    $this->fetchRequest();

    if (!isset($_data['title'])) $_data['title'] = __($this->_one);
    if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();
    
    return $this->_maestro->add($_data);
  }

  function editPrepare() {
    $this->prepare();
    $this->setFormByRecord();
  }

  function editAction($_data = array()) {
    $this->fetchRequest();

    if (!isset($_data['title'])) $_data['title'] = __($this->_one);
    if (!isset($_data['content'])) $_data['content'] = $this->form->toHTML();

    $_data['content_sidebar'] = method_exists($this, "getRelatedEditAction")
      ? $this->getRelatedEditAction(Context::getRequest(), Context::getRouter(), $_data)
      : null;
    
    return $this->_maestro->edit($_data);
  }

  function getRelatedEditAction($arrControllers) {
    $relations = ERelations::inst()->where(ERelations::TABLE1_ID, $this->table->getID())->where(ERelations::TABLE_RELATION_TYPE_ID, array(1,3,5))->findAll();
    
    if (!$relations) return null;

    $arrData = null;

    foreach ($relations AS $relation) {
      $table2 = $relation->getTable2();
      $e = $table2->getEnt();
      $entity = new $e;
      $arrData .= self::allWhere($entity, array($relation->getTableField2()->getField() => $this->record->getId()), array(
        "title" => $relation->getTitle(),
        "ctrl" => 'dynamic/tables/' . $table2->getId(),
      ));
    }

    return $arrData;
  }

  function deletePrepare() {
    $this->prepare();
  }

  function deleteAction() {
    return $this->_maestro->delete($this->record, $this->_url, false);
  }

  function viewPrepare() {
    $this->prepare();
    $this->setFormByRecord();
  }

  function viewAction($_view) {
    return $this->_maestro->view(array(), array(
      "title" => __($this->_one),
      "content" => '@T00D00',
    ));
  }
}