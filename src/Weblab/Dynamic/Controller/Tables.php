<?php

namespace Weblab\Dynamic\Controller;

use Weblab\Dynamic\Entity\Fields;

class Tables extends \SimpleController {
	function __construct() {
    parent::__construct();

    $this->_one = __('table');
    $this->_multiple = __('table');
    $this->_url = 'dynamic/tables';
  }

  static function view($record) {
    $_maestro = new \Weblab\Maestro\Controller\Maestro();
    $arrFields = Fields::inst()->where(Fields::TABLE_ID, $record->getId())->orderBy(Fields::ORDER)->findAll();

    $entity = $record->getEnt();
    $entity = new $entity();

    $arrConstants = array('slug', 'title', 'name', 'key');
    $arrHeading = array();
    foreach ($arrFields AS $field) {
      if ($field->isShown()) {
          $arrHeading[$field->getField()] = $field->getTitle();
      }
    }

    $rowBtn = array("edit", "delete");
    if ($record->getTable() == 'tables') {
      $rowBtn[] = "view";
    }

    return $_maestro->all($entity->findAll(), array(
      "title" => $record->getTitle(),
      "heading" => $arrHeading,
      "url" => array(
        "delete" => "/maestro/dynamic/tables/" . $record->getId() . "/delete",
        "edit" => "/maestro/dynamic/tables/" . $record->getId() . "/edit",
        "view" => "/maestro/dynamic/tables/view",
        "add" => "/maestro/dynamic/tables/" . $record->getId() . "/add",
        "sort" => "/maestro/dynamic/tables/" . $record->getId() . "/sort",
      ),
      "rowbtn" => $rowBtn,
      "ctrl" => $record->getTitle(),
    ));
  }

  function viewAction() {
    return self::view($this->record);
  }
}

?>