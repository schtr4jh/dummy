$.fn.datetimepicker.dates['sl'] = {
    days: ["Nedelja", "Ponedeljek", "Torek", "Sreda", "Četrtek", "petek", "Sobota", "Nedelja"],
    daysShort: ["Ned", "Pon", "Tor", "Sre", "Čet", "Pet", "Sob", "Ned"],
    daysMin: ["N", "P", "T", "S", "Č", "P", "S", "N"],
    months: ["Januar", "Februar", "Marec", "April", "Maj", "Junij", "Julij", "Avgust", "September", "Oktober", "November", "December"],
    monthsShort: ["Jan", "Feb", "Mar", "Apr", "Maj", "Jun", "Jul", "Avg", "Sep", "Okt", "Nov", "Dec"],
    today: "Danes"
};

$(document).ready(function(){
	$("a[href='#']").click(function(){
		return;
	});

  $(document).on("click", ".panel-heading", function(e){
    $(this).parent().find(".panel-heading, .panel-body").toggleClass("lfwPluginActive");
    
    return true;
  });

  $(document).on("click", ".ajaxdelete", function(){
      if (confirm("Želite izbrisat zapis?")) {
        var t = this;
        $.post($(this).attr("href"), { ajax: true }, function(data){
          if (data.success != true) {
            alert("Napaka pri brisanju podatka.");
          } else {
            $(t).parents("tr").slideUp(function(){
              $(this).remove();
            });
          }
        }, "json");
      }

      return false;
  });

  $(document).on("click", ".ajaximgdelete", function(){
      if (confirm("Želite izbrisat sliko?")) {
        var t = this;
        $.post($(this).attr("href"), { ajax: true }, function(data){
          if (data.success != true) {
            alert("Napaka pri brisanju podatka.");
          } else {
            $(t).parents(".col-sm-3").slideUp(function(){
              $(this).remove();
            });
          }
        }, "json");
      }

      return false;
  });

	$('.dtpicker').datetimepicker({
      format: 'yyyy-mm-dd hh:ii',
      weekStart: 1,
      autoclose: true,
     // language: "sl"
    });
	$('.dpicker').datetimepicker({
      format: 'yyyy-mm-dd',
      weekStart: 1,
      autoclose: true,
      minView: 'month',
      //language: "sl"
    });

    $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    
    // table listing
    /*$(".sorttable tbody").sortable({
      helper: function(e, ui) {
        ui.children().each(function() {
          $(this).width($(this).width());
        });
        return ui;
      },
      stop: function(event, ui){
        var sort = Array();
        ui.item.parent().children("tr").each(function(){
          if ($(this).find("td:first").html() == parseInt($(this).find("td:first").html()))
            sort.push($(this).find("td:first").html());
        });

        $.post(ui.item.parents("table").data("sort-url"), { sort: sort });
      }
    }).disableSelection();

    // image listing
    $(".sortimage").sortable({
      stop: function(event, ui){
        var sort = Array();
        ui.item.parent().children("div.col-sm-3").each(function(){
            sort.push($(this).data("id"));
        });

        $.post(ui.item.parents("table").data("sort-url"), { sort: sort });
      }
    }).disableSelection();*/

    $(".multiple-upload").each(function(){

      $(this).fileupload({
        url: $(this).data("upload-url"),
        dataType: 'html',
        //formData: $.parseJSON($(this).data("form-data")),
        processdone: function (e, data) {
          alert("processdone");
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css('width', progress + '%').html(progress + '%');
        }
      }).bind("fileuploaddone",function(e,data){
        $($(this).data("before")).before(data.result);
        
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    });

  // hide empty images
  $("img[src='']").hide();

  // add thumb preview
  $("input[type=file]").change(function(e){
    if ($("#" + $(this).attr("id") + "_thumb").length > 0) {
      var id = $(this).attr("id");
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#' + id + '_thumb').attr('src', e.target.result).show();
      }
      
      reader.readAsDataURL(e.currentTarget.files[0]);
    }
  });

  //
  $(document).on("click", ".enable-html-editor", function(){
    $(this).parents(".form-group").find("textarea.form-control").redactor({ minHeight: 400,paragraphy: false, convertDivs: false, css: ['http://getbootstrap.com/dist/css/bootstrap.css','/src/layouts/main/public/css/css.css']});

    return false;
  });

  $("#side-menu a").each(function(){
    if ($(this).attr("href") == document.location.pathname) {
      $(this).parent().addClass("active");
      if ($(this).parent().parent().hasClass("nav-second-level")) {
          $(this).parent().parent().parent().addClass("active");
      }
      if ($(this).parent().parent().hasClass("nav-third-level")) {
          $(this).parent().parent().parent().addClass("active");
          $(this).parent().parent().parent().parent().parent().addClass("active");
      }
    }
  });

  $('#side-menu').metisMenu();
  $('.selectpicker').selectpicker();
});