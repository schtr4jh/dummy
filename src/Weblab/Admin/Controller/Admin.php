<?php

namespace Weblab\Admin\Controller;

use LFW;
use LFW\Controller;
use LFW\Status;
use LFW\Router;
use LFW\Helpers\Auth;
use LFW\Helpers\Optimize;
use DebugBar\StandardDebugBar;

use Weblab\Menus\Entity\Menus;

class Admin extends Controller {
	function __construct() {
	    /*$gen = new LFW\Generator();
	    $gen->generateConfigFromDb("dummy");
	    $gen->generateBaseFromConfig();*/

		parent::__construct();

		$this->debugbar = new StandardDebugBar();
		$this->debugbarRenderer = $this->debugbar->getJavascriptRenderer();

		$pdo = new \DebugBar\DataCollector\PDO\TraceablePDO(\LFW\Context::getDB());
		$this->debugbar->addCollector(new \DebugBar\DataCollector\PDO\PDOCollector($pdo));

		// composer.json
		LFW\Helpers\Optimize::addFile("js", array(
			"components/jquery/jquery.min.js",
			"components/bootstrap/js/bootstrap.min.js",
			"components/html5shiv/html5shiv-built.js",
			"components/jquery-ui/ui/minified/core.min.js",
			"vendor/bootstrap-select/bootstrap-select/bootstrap-select.js",
		));

		LFW\Helpers\Optimize::addFile("css", array(
			"components/bootstrap/css/bootstrap.min.css",
			"components/bootstrap/css/bootstrap-theme.min.css",
			"components/jqueryui/themes/ui-lightness/jquery-ui.min.css",
			"vendor/fortawesome/font-awesome/css/font-awesome.min.css",
			"vendor/bootstrap-select/bootstrap-select/bootstrap-select.css",
		));

		LFW\Helpers\Optimize::addAssets(array(
			// sb-admin-2.js
			"js/sb-admin-2.js",
			"js/plugins/metisMenu/metisMenu.min.js",

			"js/chosen/v1.0.0/dev.js",
			"js/chosen/v1.0.0/conf.js",

			"js/datatables/v1.9.4/dev.js",
			"js/datatables/v1.9.4/conf.js",

			"js/redactor-js/v8.2.2/min.js",
			"js/redactor-js/v8.2.2/conf.js",

			//"js/html5shiv/v1/min.js",
			"js/bootstrap-datetimepicker/v1/min.js",
			"js/jquery-bootstrap-validation/v1.3.6/min.js",
			"js/jquery-file-upload/v8.8.5/jquery.fileupload.js",
			"js/jquery-file-upload/v8.8.5/jquery.iframe-transport.js",
			"js/maestro.js", // should it be on the end?

			// sb-admin2
			"css/sb-admin-2.css",
			"css/plugins/dataTables.bootstrap.css",
			"css/plugins/metisMenu/metisMenu.min.css",

			"css/chosen/v1.0.0/dev.css",
			"css/datatables/v1.9.4/conf.css",
			"css/fontawesome/v3.2.1/min.css",
			"css/redactor-js/v8.2.2/min.css",
			"css/bootstrap-datetimepicker/v1/dev.css",
			"css/jquery-file-upload/v8.8.5/jquery.fileupload-ui.css",
			"css/maestro.css", // should it be on the end?
		));

		LFW\Lang::lang(LFW\Lang::EN);

		if (!Auth::isLoggedIn() || !Auth::hasFlag("admin"))
			Status::internal(Router::make("login"));
	}

	function noneAction() {
		return null;
	}

	function getAdminMenu() {
		$entityMenus = new Menus();
		$adminMenu = $entityMenus->where(Menus::SLUG, 'admin_menu')->findOne();

		if (!$adminMenu) return null;

		return $adminMenu;
	}

	// function returns layout/view
	function adminAction() {
		$this->user = $_SESSION['User'];
		$this->displaymenu = TRUE;
		$this->debugbarHead = $this->debugbarRenderer->renderHead();
		$this->debugbarBody = $this->debugbarRenderer->render();
		$this->adminMenu = $this->getAdminMenu();
	}

	function withoutMenuAction() {
		$this->adminAction();

		$this->displaymenu = FALSE;
	}
}

?>