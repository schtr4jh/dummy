<?php

namespace Weblab\Auth\Controller;

use LFW;
use LFW\Controller;
use LFW\Status;
use LFW\View\Twig AS View;
use LFW\Router;
use LFW\Helpers\Auth AS LFWAuth;
use LFW\Validate;
use LFW\Debug;

use Weblab\Auth\Models\Facebook AS FBUser;
use Weblab\Users\Models\User;

class Facebook extends Auth {
	function __construct() {
		$config = array(
		      'appId' => '452088974892769',
		      'secret' => '02d1fd06941ffa3021064c58e3eac8d2',
		      'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
		      'cookie' => true,
		  );

		$this->facebook = new \Facebook($config);

		parent::__construct();
	}

	function loginAction() {
		Status::redirect($this->facebook->getLoginUrl(array(
			"scope" => "email",
			"redirect_uri" => Router::make("takelogin_fb", TRUE),
		)));
	}

	function takeloginAction() {
		$userData = $this->facebook->api("/me");

		if (!Validate::isInt($userData['id'])) {
			Status::redirect(Router::make("login_fb"));
		}

		$fbUser = new FBUser();
		$fbUser = $fbUser->findOne(array("fb_id" => $userData["id"]));

		if (!$fbUser->exists()) {
			$user = new User();
			$user = $user->findOne(array("email" => $userData["email"]));

			if (!$user->exists()) {
				$user->insert(array(
					"email" => $userData["email"],
				));
			}

			$fbUser->reset()->set(array(
				"fb_id" => $userData['id'],
				"first_name" => $userData['first_name'],
				"last_name" => $userData['last_name'],
				"link" => $userData['link'],
				"gender" => $userData['gender'],
				"username" => $userData['username'],
				"email" => $userData['email'],
				"user_id" => $user->id,
			))->insert();

			if ($fbUser->exists()) {
				LFWAuth::loginByUserID($fbUser->user_id);
			} else {
				Debug::addError(Lang::get("cannot_insert_fb"));
			}
		} else {
			LFWAuth::loginByUserID($fbUser->user_id);
		}

		Status::redirect("/");
	}

	function logoutAction() {
		$this->facebook->destroySession();
		LFWAuth::logout();
		Status::redirect("/");
	}

	function registerAction() {
	}

	function takeRegisterAction() {
	}

	function confirmRegisterAction() {
	}

	function forgotPassAction() {
	}

	function takeforgotPassAction() {
	}
}

?>