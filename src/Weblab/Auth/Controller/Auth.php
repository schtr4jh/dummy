<?php

namespace Weblab\Auth\Controller;

use LFW;
use LFW\Controller;
use LFW\Status;
use LFW\View\Twig AS View;
use LFW\Router;
use LFW\Helpers\Auth AS LFWAuth;
use LFW\Validate;
use LFW\Debug;
use LFW\Lang;
use LFW\Context;

class Auth extends Controller {
	function __construct() {
		parent::__construct();
	}
	
	function loginAction() {
		if (Context::getRequest()->isMethod(\LFW\Request::POST)) {
			$this->takeloginAction();
		}
		
		return new View("login");
	}

	function takeloginAction() {
		#if (LFWAuth::isLoggedIn())
		#	Status::redirect("/");

		if (LFWAuth::login($_POST['email'], $_POST['password'])) {
			if (LFWAuth::getUser()->getStatusId() == 1) // admin
				LFWAuth::addFlag("admin");
			else if (LFWAuth::getUser()->getStatusId() == 2)
				LFWAuth::addFlag("user");
			else
				LFWAuth::addFlag("invalid");

			if (LFWAuth::hasFlag("admin"))
				Status::redirect("/maestro");

			Status::redirect("/");
		}

		Status::redirect(Router::make("login"));
	}

	function logoutAction() {
		if (!LFWAuth::isLoggedIn())
			Status::redirect("/");

		LFWAuth::logout();

		Status::redirect("/");
	}

	function registerAction() {
		return new View("register");
	}

	function takeRegisterAction() {
		if (!isset($_POST['email']) || !isset($_POST['password']))
			throw new \Exception(Lang::get("email_pass_not_set"));

		if ($user = \Weblab\Users\Models\User::simpleInsert($_POST['email'], $_POST['password'])) {
			return new View("takeregister", array("success" => \Weblab\Mails\Controller\Mails::send("auth-register", $_POST['email'], array("user" => $user))));
		} else {
			return new View("takeregister", array("success" => FALSE));
		}
	}

	function confirmRegisterAction() {
		$user = new \Src\Weblab\Users\Models\User();

		$user = $user->findOne(array("activation" => Router::get("activation"), "id" => Router::get("id")));

		if ($user->exists()) {
			$user->update(array("activation" => NULL));
		} else {
			Debug::addError(Lang::get("user_doesnt_exist_or_activated"));
		}

		return new View("confirmregister", array("success" => $user->exists()));
	}

	function forgotPassAction() {
		return new View("forgotpass");
	}

	function takeforgotPassAction() {
		if (!isset($_POST['email']))
			throw new \Exception(Lang::get("email_not_set"));

		$user = new \Src\Weblab\Users\Models\User();

		$user = $user->findOne(array("email" => $_POST['email']));

		if (!$user->exists())
			throw new \Exception(Lang::get("user_doesnt_exist"));

		$password = substr(0, 1, sha1(microtime()));

		$success = \Weblab\Mails\Controller\Mails::send("auth-forgotpassword", $_POST['email'], array("user" => $user, "password" => $password));

		if ($success)
			$user->update(array("password" => $password));

		return new View("takeregister", array("success" => $success));
	}
}

?>