<?php

namespace Weblab\Users\Record;

class User extends \Base\Record\User {
  public function hashPassword() {
    $this->setPassword(\LFW\Helpers\Auth::makePassword($this->getPassword()));
  }
}