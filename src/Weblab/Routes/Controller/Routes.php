<?php

namespace Weblab\Routes\Controller;

use LFW;
use LFW\View\Twig AS View;
use LFW\Helpers\JSON;

use Weblab\Routes\Record\Route;

class Routes extends \SimpleController {
	function __construct() {
		parent::__construct();
	}
	
	static function getFromDB() {
		$return = array();
		return $return;
		if (($routes = self::getEntity()->findAll()) && is_array($routes))
		foreach ($routes AS $r) {
			$return[] = array(
				$r->getUrl(),
				(array)\LFW\Helpers\JSON::from($r->getSettings()),
				$r->getShort(),
			);
		}

		return $return;
	}

	function getCallTree($restrict = array("all", "add", "insert", "edit", "update", "delete")) {
    $arrViews = array();

		$dirReleasers = opendir(SRC_PATH); 
    while($dirReleasers && false !== ($releaser = readdir($dirReleasers))) { 
      if (($releaser != '.') && ($releaser != '..') && is_dir(SRC_PATH . $releaser)) {
      	$dirModules = opendir(SRC_PATH . $releaser);
		    while($dirModules && false !== ($module = readdir($dirModules))) {
		      if (($module != '.') && ($module != '..') && is_dir(SRC_PATH . $releaser . "/" . $module)) {
		      	if (!is_dir(SRC_PATH . $releaser . "/" . $module .  "/controller")) continue;

		      	$dirControllers = opendir(SRC_PATH . $releaser . "/" . $module .  "/controller");
				    while($dirControllers && false !== ($controller = readdir($dirControllers))) { 
				      if (($controller != '.') && ($controller != '..') && is_file(SRC_PATH . $releaser . "/" . $module .  "/controller/" . $controller)) { 
			        	$controller = str_replace(".php", NULL, $controller);

			          $methods = get_class_methods($releaser . '\\' . $module . '\Controller\\' . $controller);

			          if (is_array($methods))
			           	foreach ($methods AS $method) {
			           		if (strpos($method, "Action") == mb_strlen($method) - mb_strlen("Action") && !in_array(str_replace("Action", NULL, $method), $restrict)) {
			           			$arrViews[$releaser . "\\" . $module . ":" . $controller . ":" . str_replace("Action", NULL, $method)] = $releaser . "\\" . $module . ":" . $controller . ":" . str_replace("Action", NULL, $method);
			           		}
			           	}
				        } 
				    	} 
				    closedir($dirControllers);
	        } 
		    } 
		    closedir($dirModules);
      } 
    } 
    closedir($dirReleasers);

	  return $arrViews;
	}

	function addFromMemoryAction($_maestro, $_router) {
		$arrMemory = $_router->getRoutes();

		foreach ($arrMemory AS &$memory) {
			foreach ($memory AS &$m) {
				$m["json"] = JSON::to($m);
			}
		}

		// return maestro:add
		return $_maestro->block(array(
			"title" => "routo",
			"content" => new View("maestro/routes/addfrommemory", array(
				"routes" => $arrMemory,
			)),
			"url" => "/maestro/routes/insertfrommemory"
		));
	}

	function insertFromMemoryAction($_maestro) {
		$maestro = new Maestro();

		$memoryRoute = (array)JSON::from($_POST['route']);

		$route = new Route();

		$route->setTitle($memoryRoute["url"]);
		$route->setUrl($memoryRoute["url"]);

		$route->settings = \LFW\Helpers\JSON::to(array(
			"layout" => @$memoryRoute['layout'],
			"controller" => @$memoryRoute['controller'],
			"view" => @$memoryRoute['view'],
			"validate" => array(),
		));

		$route->insert();

		if ($route->exists())
			\LFW\Status::redirect("/maestro/routes/edit/" . $route->id);
		else
			\LFW\Status::redirect(-1);
	}
}

?>