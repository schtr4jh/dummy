<?php

namespace LFW;

// dynamic load from database ;-)
foreach (\Weblab\Routes\Controller\Routes::getFromDb() AS $setting) {
	if (is_string($setting[0]) && is_array($setting[1]))
		Router::add($setting[0], $setting[1], isset($setting[2]) && is_string($setting[2]) ? $setting[2] : NULL);
}

Router::add(
	"/maestro/routes/addfrommemory",
	array(
		"layout" => "Weblab\\Admin",
		"controller" => "Weblab\\Routes",
		"view" => "addfrommemory",
	)
);

Router::add(
	"/maestro/routes/insertfrommemory",
	array(
		"layout" => "Weblab\\Admin",
		"controller" => "Weblab\\Routes",
		"view" => "insertfrommemory",
	)
);

?>