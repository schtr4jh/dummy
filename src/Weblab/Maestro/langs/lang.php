<?php

namespace Weblab\Maestro\Langs;

use LFW\Lang;

/*class Lang extends \LFW\Lang {
	function __construct() {
		self::set("en", "admin_panel", "admin panel");
		self::set("si", "admin_panel", "nadzorna plošča");
	}
}*/

Lang::set(Lang::EN, "admin_panel", "Admin panel");
Lang::set(Lang::SI, "admin_panel", "Nadzorna plošča");

Lang::set(Lang::EN, "sort", "Sort");
Lang::set(Lang::SI, "sort", "Razvrsti");

Lang::set(Lang::EN, "next", "Next");
Lang::set(Lang::SI, "next", "Naprej");

Lang::set(Lang::EN, "add", "Add");
Lang::set(Lang::SI, "add", "Dodaj");

Lang::set(Lang::EN, "edit", "Edit");
Lang::set(Lang::SI, "edit", "Uredi");

Lang::set(Lang::EN, "delete", "Delete");
Lang::set(Lang::SI, "delete", "Izbriši");

Lang::set(Lang::EN, "view", "View");
Lang::set(Lang::SI, "view", "Poglej");

Lang::set(Lang::EN, "save", "Save");
Lang::set(Lang::SI, "save", "Shrani");

Lang::set(Lang::EN, "list", "List");
Lang::set(Lang::SI, "list", "Seznam");

Lang::set(Lang::EN, "check", "Check: ");
Lang::set(Lang::SI, "check", "Preveri: ");

Lang::set(Lang::EN, "alert_success_insert", "Successfully added.");
Lang::set(Lang::SI, "alert_success_insert", "Zapis je bil uspešno dodan.");

Lang::set(Lang::EN, "alert_success_update", "Successfully updated.");
Lang::set(Lang::SI, "alert_success_update", "Zapis je bil uspešno posodobljen.");

Lang::set(Lang::EN, "alert_success_delete", "Successfully deleted.");
Lang::set(Lang::SI, "alert_success_delete", "Zapis je bil uspešno izbrisan.");

Lang::set(Lang::EN, "alert_error_insert", "Error inserting data.");
Lang::set(Lang::SI, "alert_error_insert", "Prišlo je do napake pri dodajanju zapisa.");

Lang::set(Lang::EN, "alert_error_update", "Error updating data.");
Lang::set(Lang::SI, "alert_error_update", "Prišlo je do napake pri urejanju zapisa.");

Lang::set(Lang::EN, "alert_error_delete", "Error deleting data.");
Lang::set(Lang::SI, "alert_error_delete", "Prišlo je do napake pri izbrisu zapisa.");

Lang::set(Lang::EN, "invalid_id", "Invalid ID");
Lang::set(Lang::SI, "invalid_id", "Napačen ID.");

?>