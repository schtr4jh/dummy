<?php

namespace Weblab\Maestro\Controller;

use LFW;
use LFW\Controller;
use LFW\Validate;
use LFW\View\Twig AS View;
use LFW\Router;
use LFW\Assets;
use LFW\Status;
use LFW\Settings;
use LFW\Helpers\JSON;
use LFW\Helpers\Auth;
use LFW\DB;
use LFW\Lang;
use LFW\Debug;

use Weblab\Logs\Record\Log;

class Maestro extends Controller {
	protected $view = null;
	protected $data = array();
	protected $extends = 'weblab/admin/views/admin.twig';

	function __construct() {
		parent::__construct();
	}

	function setView($view) {
		$this->view = $view;
	}

	function buildMaestro() {
		$view = new View($this->view);
		$view->addData($this->data);

		return $view;
	}

	// old

	function indexAction() {
		return new View("index");
	}

	function all($table, $conf = array()) {
		if (!isset($conf['ctrl'])) {
			$db = debug_backtrace();
			$spacename = explode("\\", get_class($db[1]['object']));
			$ctrl = strtolower(
				$spacename[count($spacename)-1] == $spacename[count($spacename)-3]
					? $spacename[count($spacename)-3]
					: ($spacename[count($spacename)-3] . "/" . $spacename[count($spacename)-1])
				);
		} else {
			$ctrl = $conf['ctrl'];
		}

		$conf["url"]["sort"] = isset($conf["url"]["sort"])
			? $conf["url"]["sort"]
			: ("/maestro/" . $ctrl . "/sort");

		$conf["url"]["add"] = isset($conf["url"]["add"])
			? $conf["url"]["add"]
			: ("/maestro/" . $ctrl . "/add");

		$conf["url"]["edit"] = isset($conf["url"]["edit"])
			? $conf["url"]["edit"]
			: ("/maestro/" . $ctrl . "/edit");

		$conf["url"]["view"] = isset($conf["url"]["view"])
			? $conf["url"]["view"]
			: ("/maestro/" . $ctrl . "/view");

		$conf["url"]["delete"] = isset($conf["url"]["delete"])
			? $conf["url"]["delete"]
			: ("/maestro/" . $ctrl . "/delete");

		$conf["rowbtn"] = isset($conf["rowbtn"])
			? $conf["rowbtn"]
			: array("edit", "delete", "view");
			
		$conf["headbtn"] = isset($conf["headbtn"])
			? $conf["headbtn"]
			: array("add");

		$finalTable = array();
		foreach ($table AS $i => $row) {
			$temp = array();

			if (!is_array($row)){
				$rowArr = $row->__toArray();

				foreach ($rowArr AS $key => $val) {
					if (substr($key, -3) == "_id" && in_array($key, array_keys($conf['heading']))) {
						$v = $row->{"get".\LFW\Core::toCamel(substr(ucfirst($key), 0, -3))}();
						$rowArr[$key] = $v ? $v->__getMain() : $v;
					}
				}
			} else {
				$rowArr = $row;
			}

			foreach ($conf["heading"] AS $hkey => $hval) {
				$temp[$hkey] = $rowArr[$hkey];
			}

			if (is_array($row) && isset($row["rowbtn"]))
				$temp["rowbtn"] = $row["rowbtn"];

			$finalTable[] = $temp;
		}

		return new View("all", array(
			"ext" => array_key_exists("extends", $conf) ? $conf["extends"] : false,
			"data" => $finalTable,
			"conf" => $conf,
			"breadcrumbs" => array(
				array(
					"title" => "Maestro",
					"href" => "/maestro",
				),
				array(
					"title" =>  \LFW\Core::toCamel($ctrl),
				),
			),
		));
	}

	function allForeign($table, $conf = array()) {
		if (!isset($conf['ctrl'])) {
			$db = debug_backtrace();
			$spacename = explode("\\", get_class($db[1]['object']));
			$ctrl = strtolower(
				$spacename[count($spacename)-1] == $spacename[count($spacename)-3]
					? $spacename[count($spacename)-3]
					: ($spacename[count($spacename)-3] . "/" . $spacename[count($spacename)-1])
				);
		} else {
			$ctrl = $conf['ctrl'];
		}

		$conf["url"]["sort"] = isset($conf["url"]["sort"])
			? $conf["url"]["sort"]
			: ("/maestro/" . $ctrl . "/sort");

		$conf["url"]["add"] = isset($conf["url"]["add"])
			? $conf["url"]["add"]
			: ("/maestro/" . $ctrl . "/add");

		$conf["url"]["edit"] = isset($conf["url"]["edit"])
			? $conf["url"]["edit"]
			: ("/maestro/" . $ctrl . "/edit");

		$conf["url"]["delete"] = isset($conf["url"]["delete"])
			? $conf["url"]["delete"]
			: ("/maestro/" . $ctrl . "/delete");

		$conf["rowbtn"] = isset($conf["rowbtn"])
			? $conf["rowbtn"]
			: array("edit", "delete");
			
		$conf["headbtn"] = isset($conf["headbtn"])
			? $conf["headbtn"]
			: array("add");

		$finalTable = array();
		foreach ($table AS $i => $row) {
			$temp = array();

			if (!is_array($row)){
				$rowArr = $row->__toArray();

				foreach ($rowArr AS $key => $val) {
					if (substr($key, -3) == "_id" && in_array($key, array_keys($conf['heading']))) {
						$v = $row->{"get".\LFW\Core::toCamel(substr(ucfirst($key), 0, -3))}();
						$rowArr[$key] = $v ? $v->__getMain() : $v;
					}
				}
			} else {
				$rowArr = $row;
			}

			foreach ($conf["heading"] AS $hkey => $hval) {
				$temp[$hkey] = $rowArr[$hkey];
			}

			if (is_array($row) && isset($row["rowbtn"]))
				$temp["rowbtn"] = $row["rowbtn"];

			$finalTable[] = $temp;
		}

		return new View("all_foreign", array(
			"ext" => array_key_exists("extends", $conf) ? $conf["extends"] : false,
			"data" => $finalTable,
			"conf" => $conf,
			"breadcrumbs" => array(
				array(
					"title" => "Maestro",
					"href" => "/maestro",
				),
				array(
					"title" =>  \LFW\Core::toCamel($ctrl),
				),
			),
		));
	}

	function sort($table, $conf = array()) {
		$ctrl = end(explode("\\", Router::get("controller")));

		foreach ($table AS $i => $row) {
			$temp = array();

			foreach ($conf["heading"] AS $hkey => $hval) {
				$temp[$hkey] = $row[$hkey];
			}

			$table[$i] = $temp;
		}

		return new View("sort", array(
			"ext" => isset($conf["extends"]) ? $conf["extends"] : false,
			"data" => $table,
			"conf" => $conf,
			"breadcrumbs" => array(
				array(
					"title" => "Maestro",
					"href" => "/maestro",
				),
				array(
					"title" =>  \LFW\Core::toCamel($ctrl),
					"href" => "/maestro/" . $ctrl,
				),
				array(
					"title" => __("sort"),
				),
			),
		));
	}

	function updatesort($data, $model, $pos = "position") {
		foreach ($data AS $position => $id) {
			$sql = "UPDATE " . $model->getTable() . " " .
				"SET " . $pos . " = " . ((int)$position + 1) . " " .
				"WHERE id = " . ((int)$id);
			DB::query($sql);
		}

		return JSON::to(array(
			"success" => TRUE,
		));
	}

	function add($conf = array()) {
		$ctrl = substr(strrchr(Router::get("controller"), "\\"), 1);

		$conf["url"]["insert"] = "/dev.php" . (isset($conf["url"]["insert"])
			? $conf["url"]["insert"]
			: ("/maestro/" . $ctrl . "/insert"));

		return new View("add", array(
			"ext" => isset($conf["extends"]) ? $conf["extends"] : false,
			"conf" => $conf,
			"breadcrumbs" => array(
				array(
					"title" => "Maestro",
					"href" => "/maestro",
				),
				array(
					"title" =>  \LFW\Core::toCamel($ctrl),
					"href" => "/maestro/" . $ctrl,
				),
				array(
					"title" => __("add"),
				),
			),
		));
	}

	function i18nAddPrepare($i18n) {
		$arri18n = array();
		$lang = array(1 => "Slo", 2 => "Eng", 3 => "Ita");
		foreach ($lang AS $langId => $langTitle) {
			$obj = new $i18n;
			$obj->forceSet(array("lang_id" => $langId, "_tablang" => array("id" => $langId, "title" => $langTitle)));
			$arri18n[] = $obj;
		}
		return $arri18n;
	}

	function i18nAdd($conf) {
		return new View("i18nadd", array(
			"conf" => $conf,
		));
	}

	function insert($record, $plural, $redirect = TRUE, $callback = NULL) {
		if (!$record || ($validate = $record->validate()) && ($insert = $record->insert())) {
			if ($redirect)
				Debug::addSuccess(__("alert_success_insert"));

			//$log = new Log;
			//$log->set(array("content" => "User " . Auth::getUser()->getEmail() . " created " . $plural . "#" . $record->id()));
			//$log->insert();
		}

		if ($redirect && !$validate)
			Debug::addError(__("alert_error_insert") . " (validate)");
		else if ($redirect && !$insert)
			Debug::addError(__("alert_error_insert") . " (insert)");
		else if (is_callable($callback))
			$callback($record);

		if (isset($_POST['ajax']))
			return JSON::to(array(
				"success" => !!$insert,
			));

		if ($redirect)
			Status::redirect("/maestro/" . $plural . ($insert ? "/edit/" . $insert->id() : "/add"));

		return $insert;
	}

	function edit($conf = array()) {
		$ctrl = substr(strrchr(Router::get("controller"), "\\"), 1);

		$conf["url"]["update"] = isset($conf["url"]["update"])
			? $conf["url"]["update"]
			: ("/maestro/" . $ctrl . "/update");

		return new View("edit", array(
			"ext" => isset($conf["extends"]) ? $conf["extends"] : false,
			"conf" => $conf,
			"breadcrumbs" => array(
				array(
					"title" => "Maestro",
					"href" => "/maestro",
				),
				array(
					"title" =>  \LFW\Core::toCamel($ctrl),
					"href" => "/maestro/" . $ctrl,
				),
				array(
					"title" => __("edit"),
				),
			),
		));
	}

	function i18nEditPrepare($parentEntity, $record, $entity) {
		$arri18n = array();
		$lang = array(1 => "Slo", 2 => "Eng", 3 => "Ita");
		foreach ($lang AS $langId => $langTitle) {
			$one = $entity->where(array("lang_id" => $langId, $parentEntity->getI18nForeign() => $record->id()))->findOne();
			
			if ($one)
				$arri18n[] = $one->forceSet(array("_tablang" => array("id" => $langId, "title" => $langTitle)));
		}
		return $arri18n;
	}

	function i18nEdit($conf) {
		return new View("i18nedit", array(
			"conf" => $conf,
		));
	}

	function update($record, $plural, $redirect = TRUE, $callback = NULL) {
		if (!$record || !Validate::isInt($record->getID())) {
			Debug::addWarning(__("invalid_id") . " " . $record->getID());

			$log = new Log(array("content" => "User " . Auth::getUser()->getEmail() . " failed editing " . $plural . "#" . $record->getId() . " (invalid ID)"));
			$log->insert();
				
			if ($redirect) {
				Status::redirect(-1);
			}
		} else if (($validate = $record->validate()) && ($update = $record->update())) {
			Debug::addSuccess(__("alert_success_update"));

			//$log = new Log(array("content" => "User " . Auth::getUser()->getEmail() . " edited " . $plural . "#" . $record->getId()));
			//$log->insert();
		}

		if (!$validate)
			Debug::addError(__("alert_error_update") . " " . __("check") . $model->getValidateErrorsHTML());
		else if ($update === false)
			Debug::addError("Update? " . print_r(DB::inst()->errorInfo(), true));
		else if (is_callable($callback))
			$callback($record);

		if (isset($_POST['ajax']))
			return JSON::to(array(
				"success" => !!$update,
			));

		if ($redirect)
			Status::redirect("/maestro/" . $plural . "/edit/" . $record->getId());
		
		return $update;
	}

	function delete($record, $plural, $redirect = FALSE, $callback = NULL, $precall = NULL) {
		if (is_callable($precall))
			$precall($record);

		if (!$record || !Validate::isInt($record->getId())) {
			Debug::addWarning(__("invalid_id") . print_r($record, TRUE));
			Status::redirect(-1);
		} else if ($delete = $record->delete()) {
			if ($redirect)
				Debug::addSuccess(__("alert_success_delete"));
		}

		if (!$delete) {
			if ($redirect)
				Debug::addError(__("alert_error_delete"));
		} else if (is_callable($callback))
			$callback($record);

		if ($this->getRequest()->isAjax())
			return JSON::to(array(
				"success" => !!$delete,
			));
		
		if ($redirect)
			Status::redirect("/maestro/" . $plural);
		
		return $delete;
	}

	function view($row, $conf = array()) {
		$data = array(
			"ext" => isset($conf["extends"]) ? $conf["extends"] : false,
			"data" => $row,
			"conf" => $conf,
			"breadcrumbs" => array(
				array(
					"title" => "Maestro",
					"href" => "/maestro",
				),
			),
		);

		$allControllers = array();
		$explodeController = explode("\\", Router::get("controller"));
		foreach (explode(":", end($explodeController)) AS $controller) {
			$allControllers[] = $controller;

			$data["breadcrumbs"][] = array(
				"title" =>  \LFW\Core::toCamel($controller),
				"href" => "/maestro/" . strtolower(implode("/", $allControllers)),
			);
		}

		$data["breadcrumbs"][] = array(
			"title" => __("view"),
		);

		return new View("view", $data);
	}

	function custom($content) {
		return new View("custom", $content);
	}

	function block($conf = array()) {
		$ctrl = end(explode("\\", Router::get("controller")));

		$conf["url"] = isset($conf["url"])
			? $conf["url"]
			: "#";

		return new View("block", array(
			"conf" => $conf,
		));
	}
}

?>