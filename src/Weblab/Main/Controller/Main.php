<?php

namespace Weblab\Main\Controller;

use LFW;
use LFW\View\Twig AS View;
use LFW\Controller;

// main layout
class Main extends Controller {
	function __construct() {
		parent::__construct();

		// composer.json
		LFW\Helpers\Optimize::addFile("js", array(
			"vendor/components/jquery/jquery.min.js",
			"vendor/components/bootstrap/js/bootstrap.min.js",
			"vendor/afarkas/html5shiv/src/html5shiv.js",
			"vendor/components/jqueryui/ui/minified/jquery-ui.min.js",
		));

		LFW\Helpers\Optimize::addFile("css", array(
			"vendor/components/bootstrap/css/bootstrap.min.css",
			"vendor/components/bootstrap/css/bootstrap-theme.min.css",
			"vendor/components/jqueryui/themes/ui-lightness/jquery-ui.min.css",
			"vendor/fortawesome/font-awesome/css/font-awesome.min.css",
		));

		LFW\Helpers\Optimize::addAssets(array(
			//"css/maestro.css", // should it be on the end?
			"modern-business/css/modern-business.css",
			"modern-business/js/modern-business.js",
			"respond/respond.min.js"
		));
	}

	function mainAction() {
		return array();
	}
}

?>