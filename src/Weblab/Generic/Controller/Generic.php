<?php

namespace Weblab\Generic\Controller;

use LFW;
use LFW\View\Twig AS View;
use LFW\Controller;

// main layout
class Generic extends Controller {
  function __construct() {
    //$gen = new LFW\Generator();
    //$gen->generateConfigFromDb("dummy");
    //$gen->generateBaseFromConfig();

    parent::__construct();

    // composer.json
    LFW\Helpers\Optimize::addFile("js", array(
      "vendor/components/jquery/jquery.min.js",
      "vendor/components/bootstrap/js/bootstrap.min.js",
      "vendor/afarkas/html5shiv/src/html5shiv.js",
      "vendor/components/jqueryui/ui/minified/jquery-ui.min.js",
    ));

    LFW\Helpers\Optimize::addFile("css", array(
      "vendor/components/bootstrap/css/bootstrap.min.css",
      "vendor/components/bootstrap/css/bootstrap-theme.min.css",
      "vendor/components/jqueryui/themes/ui-lightness/jquery-ui.min.css",
      "vendor/fortawesome/font-awesome/css/font-awesome.min.css",
    ));
  }

  // generic layout
  function genericAction() {
    $layout = 1;
    $db = $this->getDB();

    $sqlLayoutActions = "SELECT 
        a.spacename,
        lv.slug AS variable 
      FROM actions a
      INNER JOIN layout_actions la ON (la.action_id = a.id) 
      INNER JOIN layouts l ON (l.id = la.layout_id)
      INNER JOIN layout_variables lv ON (lv.id = la.layout_variable_id)
      INNER JOIN routes r ON (r.layout_id = l.id)
      WHERE l.id = " . $layout . "
      AND r.route = :route
      ORDER BY la.position";
    $pLayoutActions = $db->prepare($sqlLayoutActions);
    $qLayoutActions = $pLayoutActions->execute(array("route" => $this->getRouter()->get("url")));
    $arrLayoutsActions = $pLayoutActions->fetchAll();

    $arrVariables = array();
    if ($qLayoutActions)
    foreach ($arrLayoutsActions AS $rLayoutAction) {
      if (!isset($arrVariables[$rLayoutAction->variable])) $arrVariables[$rLayoutAction->variable] = array();

      if (strpos($rLayoutAction->spacename, "::")) $rLayoutAction->spacename = explode("::", $rLayoutAction->spacename);
      else {
        $rLayoutAction->spacename = array($rLayoutAction->spacename, array_pop(explode("/", $rLayoutAction->spacename)));
      }

      $output = null;
      try {
        $output = \LFW\Core::loadView($rLayoutAction->spacename[1], array('_data' => array('slug' => 'footer')), $this->getController($rLayoutAction->spacename[0]));

        if (is_array($output)) {
          foreach ($output AS $key => $val) {
            $arrVariables[$key][] = $val;
          }
        } else {
          if (is_string($output) || is_numeric($output) || is_null($output) || is_bool($output)) {
            $output = (string)$output;
          } else if ($output instanceof \LFW\View) {
            $output = $output->autoparse();
          } else if (method_exists($output, '__toString')) {
            $output = $output->__toString();
          } else {
            die("what to do with view?");
          }

          $arrVariables[$rLayoutAction->variable][] = $output;
        }
      } catch (Exception $e) {
        var_dump($e);die();
      }
    }

    foreach ($arrVariables AS &$arrVars) {
      $arrVars = implode($arrVars);
    }

    //var_dump($arrVariables);
    $yaml = new \Symfony\Component\Yaml\Yaml();
    $file = __DIR__ . "/../config/layout.yml";

    // @T00D00 - solve /../ !!!
    if (is_file(APP_PATH . "src" . DS . str_replace('\\', '/', strtolower(__NAMESPACE__)) . "/../config/layout.yml"))
      $file = APP_PATH . "src" . DS . str_replace('\\', '/', strtolower(__NAMESPACE__)) . "/../config/layout.yml";

    $arrVariables["structure"] = $yaml->parse(file_get_contents($file));
    $arrVariables["structure"] = $arrVariables["structure"]["structure"];

    $arrVariables['test'] = $this->getController("\Weblab\Shop\Controller\BasicShop")->indexAction();
    
    return new View("generic", $arrVariables);
  }

  // generic views
  function viewAction() {
    $db = $this->getDB();

    $sqlLayoutActions = "SELECT 
        a.spacename,
        lv.slug AS variable 
      FROM actions a
      INNER JOIN route_actions ra ON (ra.action_id = a.id) 
      INNER JOIN routes r ON (r.id = ra.route_id)
      LEFT OUTER JOIN layout_variables lv ON (lv.id = ra.layout_variable_id)
      WHERE r.route = :route
      ORDER BY ra.position";
    $qLayoutActions = $db->prepare($sqlLayoutActions);
    $qLayoutActions->execute(array(":route" => $this->getRouter()->get("url")));

    $arrVariables = array();
    if ($qLayoutActions) {
      $arrLayoutsActions = $qLayoutActions->fetchAll();
      foreach ($arrLayoutsActions AS $rLayoutAction) {
        if (!isset($arrVariables[$rLayoutAction->variable])) $arrVariables[$rLayoutAction->variable] = array();

        if (strpos($rLayoutAction->spacename, "::")) {
          $rLayoutAction->spacename = explode("::", $rLayoutAction->spacename);
        } else {
          $rLayoutAction->spacename = array($rLayoutAction->spacename, array_pop(explode("/", $rLayoutAction->spacename)));
        }

        $output = null;
        try {
          $output = \LFW\Core::loadView($rLayoutAction->spacename[1], array('slug' => 'footer'), $this->getController($rLayoutAction->spacename[0]));

          if (is_array($output)) {
            foreach ($output AS $key => $val) {
              $arrVariables[$key][] = $val;
            }
          } else {
            if (is_string($output) || is_numeric($output) || is_null($output) || is_bool($output)) {
              $output = (string)$output;
            } else if ($output instanceof \LFW\View) {
              $output = $output->autoparse();
            } else {
              die("what to do with view?");
            }

            $arrVariables[$rLayoutAction->variable][] = $output;
          }
        } catch (Exception $e) {
          var_dump($e);die();
        }
      }
    }

    foreach ($arrVariables AS &$arrVars) {
      $arrVars = implode($arrVars);
    }

    //var_dump($arrVariables);

    // parse variables into generic layout
    return $arrVariables;
  }

  function testAction() {
    return array("content" => "a");
  }

  function menuAction() {
    return new View("menu");
  }
}

?>